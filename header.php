<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Clínica Taira| Home </title>


	<!-- Font awesome css -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- owl Carousel css -->
	<link rel="stylesheet" href="css/theme_default.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
		<link rel="stylesheet" href="rs-plugin/css/settings.css" media="screen" />
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/slicknav.min.css">
	
	<!-- PrettyPhoto css -->
	<link rel="stylesheet" href="css/prettyPhoto.css">
	
	<!-- DatePicker css -->
	<link rel="stylesheet" href="css/datepicker.css">
	
	<!-- Tabs css -->
	<link rel="stylesheet" href="css/easy-responsive-tabs.css">
	
	<!-- Accordian css -->
	<link rel="stylesheet" href="css/smk-accordion.css">
	
	<!-- Custom css -->
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/responsive.css">
	
    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!-- This Template Is Fully Coded By Shakhawat H. from codingcouples.com -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	</head>
  <body>
		<div class="main_header">
			<!-- Start Header Top -->
			<div class="header_top_area">
				<div class="container">
					<div class="header_top">
						<ul> 
						<!--
							<li><a href="mailto:info@medicare.com"><i class="fa fa-envelope"></i> info@medicare.com</a></li>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube-play"></i></a></li> -->
						</ul>
					</div>
				</div>	
			</div>
			<!-- End Header Top-->
			
			<!-- Start Header -->
			<div class="header">
				<div class="container">
					<div class="logo_contact_info_part">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<!--<div class="logo">
								<a href="index.html"><img src="images/logo.png" height="50" width= alt="logo" /></a>
							</div> -->
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="cont_info">
								<div class="week">
									<!--<img src="images/date_icon.png" alt="date" />
									<p>Mon to Sat <br><span>9 am to 8 PM</span> </p> -->
								</div>
								<div class="call_us">
									<img src="images/mobile_icon.png" alt="date" />
									<p>Ligue para nós <br><span>933526316</span> </p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="booking_cart">
								<div class="cart_count">
									<!--<a href="#"><i class="fa fa-shopping-cart"></i> <span class="cart_co">2</span></a> -->
								</div>
								<div class="book_now">
									<a href="#">Agende uma Consulta</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Header -->	
		
			<!-- Start Menu area -->
			<div class="nav_search_area hidden-xs">
				<div class="container">
					<div class="menu_area">
						<div class="col-md-9">	
							<div class="nav_part">
								<nav class="navbar navbar-default">
									<div class="container-fluid">
										<div class="row">
										<div class="main_menu">
										<!-- Brand and toggle get grouped for better mobile display -->
										<div class="navbar-header">
										  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
											<span class="sr-only">Toggle navigation</span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
											<span class="icon-bar"></span>
										  </button>	
										</div>

										<!-- Collect the nav links, forms, and other content for toggling -->
										<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
										  <ul class="nav navbar-nav">
											<li class="active"><a href="index.html">Home</a></li>
											<li><a href="doctors.html">Doutores</a></li>
											<li><a href="service.html">Serviços</a>
								                <ul class="submenu">
                                                    <li><a href="service2.html">Services style 2</a></li>
                                                    <li><a href="service-details.html">Services details</a></li>
                                                </ul>
											</li>
											<li><a href="contact.html">Contato</a></li>
											<li><a href="appointment.html">Agendamento</a>
                                               
                                            </li>
										  </ul>
										</div><!-- /.navbar-collapse -->
										</div>
										</div>
									</div><!-- /.container-fluid -->
								</nav>
							</div>
						</div>
						<div class="col-md-3">
							<div class="search">
								<form class="navbar-form navbar-left" role="search">
									<div class="form-group">
										<!--<input type="text" id="src_field" class="form-control" placeholder="Search">
										<input type="submit" id="src_btn" value="Search"/>
										-->
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Menu area -->
		</div>
	