
		<!-- Start Services  -->
		<div class="services_area">
			<div class="container">
				<div class="serv_tabs">
					<div class="serv_text_part">
						<h2>Nossos serviços</h2>
						<p><img src="images/our_doc_heart.png" alt="Services" /></p>
					</div>
					<div role="tabpanel" class="sev_tables">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="cardiology_tab" role="presentation"><a href="#cardiology" aria-controls="cardiology" role="tab" data-toggle="tab">Cardiology</a></li>
							<li class="dentalcare_tab active" role="presentation" ><a href="#dentalcare" aria-controls="dentalcare" role="tab" data-toggle="tab">Dental Care</a></li>
							<li class="eyecare_tab" role="presentation"><a href="#eyecare" aria-controls="eyecare" role="tab" data-toggle="tab">Eye Care</a></li>
							<li class="neurology_tab" role="presentation"><a href="#neurology" aria-controls="neurology" role="tab" data-toggle="tab">Neurology</a></li>
							<li class="kidneys_tab" role="presentation"><a href="#kidneys" aria-controls="kidneys" role="tab" data-toggle="tab">Kidney Sergery</a></li>
							<li class="ecall_tab" role="presentation"><a href="#ecall" aria-controls="ecall" role="tab" data-toggle="tab">Emergency call</a></li>
						</ul>

					  <!-- Tab panes -->
					  <div class="tab-content serv_expanel">
						<div role="tabpanel" class="tab-pane" id="cardiology">
							<div class="">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_cont">
											<h1>Cardiology</h1>
											<img src="images/our_doc_heart.png" alt="Heart" />
											<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemen augue. Aliquam vitae enim risus, at laoreet metus.elemen siquam elementum quisaugue.</p>
											<ul>
												<li>Qualified Staff of Doctors</li>
												<li>Save your Money and Time</li>
												<li>24x7 Emergency Services</li>
												<li>Easy & Affordable Billing</li>
											</ul>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_slide">
											<img src="images/dental_slide1.png" alt="dental" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane active" id="dentalcare">
							<div class="">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_cont">
											<h1>Dental Care</h1>
											<img src="images/our_doc_heart.png" alt="Heart" />
											<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemen augue. Aliquam vitae enim risus, at laoreet metus.elemen siquam elementum quisaugue.</p>
											<ul>
												<li>Qualified Staff of Doctors</li>
												<li>Save your Money and Time</li>
												<li>24x7 Emergency Services</li>
												<li>Easy & Affordable Billing</li>
											</ul>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_slide">
											<img src="images/dental_slide1.png" alt="dental" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="eyecare">
							<div class="">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_cont">
											<h1>Eye Care</h1>
											<img src="images/our_doc_heart.png" alt="Heart" />
											<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemen augue. Aliquam vitae enim risus, at laoreet metus.elemen siquam elementum quisaugue.</p>
											<ul>
												<li>Qualified Staff of Doctors</li>
												<li>Save your Money and Time</li>
												<li>24x7 Emergency Services</li>
												<li>Easy & Affordable Billing</li>
											</ul>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_slide">
											<img src="images/dental_slide1.png" alt="dental" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="neurology">
							<div class="">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_cont">
											<h1>Neurology</h1>
											<img src="images/our_doc_heart.png" alt="Heart" />
											<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemen augue. Aliquam vitae enim risus, at laoreet metus.elemen siquam elementum quisaugue.</p>
											<ul>
												<li>Qualified Staff of Doctors</li>
												<li>Save your Money and Time</li>
												<li>24x7 Emergency Services</li>
												<li>Easy & Affordable Billing</li>
											</ul>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_slide">
											<img src="images/dental_slide1.png" alt="dental" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="kidneys">
							<div class="">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_cont">
											<h1>Kidney </h1>
											<img src="images/our_doc_heart.png" alt="Heart" />
											<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemen augue. Aliquam vitae enim risus, at laoreet metus.elemen siquam elementum quisaugue.</p>
											<ul>
												<li>Qualified Staff of Doctors</li>
												<li>Save your Money and Time</li>
												<li>24x7 Emergency Services</li>
												<li>Easy & Affordable Billing</li>
											</ul>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_slide">
											<img src="images/dental_slide1.png" alt="dental" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="ecall">
							<div class="">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_cont">
											<h1>Emergency call</h1>
											<img src="images/our_doc_heart.png" alt="Heart" />
											<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemen augue. Aliquam vitae enim risus, at laoreet metus.elemen siquam elementum quisaugue.</p>
											<ul>
												<li>Qualified Staff of Doctors</li>
												<li>Save your Money and Time</li>
												<li>24x7 Emergency Services</li>
												<li>Easy & Affordable Billing</li>
											</ul>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="dent_tab_slide">
											<img src="images/dental_slide1.png" alt="dental" />
										</div>
									</div>
								</div>
							</div>
						</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Services  -->