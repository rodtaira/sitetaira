<?php

 $array_banners = $Cliente->getBanners();

 include(ROOT_SITE."/header.php");



?>


		
			<!-- Start Slider part -->
			<div class="slider_area">
                <div class="tp-banner" >
                        <ul>
                            <!-- SLIDE  -->
                            <?php

                            foreach($array_banners as $Banner ) {
                            	echo "<li data-transition='random' data-slotamount='7' data-masterspeed='1500' >
                                
                                <img src='".THUMB."?imagem={$Banner->arquivo}&w=1800&h=730'  alt='slidebg1'  data-bgfit='cover' data-bgposition='center top' data-bgrepeat='no-repeat'>
                             
                                </li>" ;
                            	
                 

                            }
                            ?>
                            
                        </ul>
                    </div>
			</div>
			<!-- End Slider part -->		
		
			<!-- Start Welcome -->
			<section class="welcome_area">
				<div class="container">
					<div class="welcome">
						<div class="col-md-5 col-sm-5">
							<div class="doctor_img">
								<img src="images/doctors.jpg" alt="Doctors" />
							</div>
						</div>
						<div class="col-md-7 col-sm-7">
							<div class="doctor_text">
								<h1>Welcome</h1>
								<h2>To <img src="images/heart_icon.png" alt="heart" /> <strong>Medi</strong>care</h2>
								<?php
									echo $Cliente->historico; 
								?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Welcome -->
		
			<!-- Start Book Appointment  -->
			<section class="book_appointment">
				<div class="col-md-4 col-sm-4 appskype">
					<div class="boo_apppinment_left">
						<img src="images/book_appo.png" alt="Book Appointment" />
						<h1>Agende </h1>
						<h2>Uma COnsulta</h2>
					</div>
				</div>
				<div class="col-md-8 col-sm-8 appblack">
					<div class="booking_form">
						<form action="index.html">
							<div class="form_top">
								<input type="text" class="fname" placeholder="Nome Completo"/>
								<input type="email" class="fname" placeholder="Email"/>
								<input type="text" class="fname" placeholder="Telefone"/>
							</div>
							<div class="form_bottom">
								<input type="text" class="datepicker dtefield" placeholder="Booking Date"/>
								<input type="text" class="message" placeholder="Mensagem" />
							</div>
							<input type="submit" class="mess_submit" value="Agende uma data"/>
						</form>
					</div>
				</div>
			</section>	
			<!-- End Book Appointment  -->		
	
		<!-- Start Services  -->
		<?php
  
 			include(ROOT_SITE."/services.php");


		?>

		<!-- End Services  -->
		
		<!-- Start Our Doctors  -->
		<div class="our_doctors_area">
			<div class="container">
				<div class="our_doc_list">
					<div class="our_doc">
						<div class="doc_text_part">
							<h2>Our Doctors</h2>
							<p><img src="images/our_doc_heart.png" alt="Services" /></p>						
						</div>
						<div class="doctor_carousel">
							<div class="items">
								<img src="images/doctor1.png" alt="doctor" />
								<div class="doc_overly">
									<h3>John <span>Deshmukh</span></h3>
									<h4>Dental Specialist</h4>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="items">
								<img src="images/doctor6.png" alt="doctor" />
								<div class="doc_overly">
									<h3>John <span>Deshmukh</span></h3>
									<h4>Dental Specialist</h4>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="items">
								<img src="images/doctor2.png" alt="doctor" />
								<div class="doc_overly">
									<h3>John <span>Deshmukh</span></h3>
									<h4>Dental Specialist</h4>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="items">
								<img src="images/doctor3.png" alt="doctor" />
								<div class="doc_overly">
									<h3>John <span>Deshmukh</span></h3>
									<h4>Dental Specialist</h4>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="items">
								<img src="images/doctor4.png" alt="doctor" />
								<div class="doc_overly">
									<h3>John <span>Deshmukh</span></h3>
									<h4>Dental Specialist</h4>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="items">
								<img src="images/doctor5.png" alt="doctor" />
								<div class="doc_overly">
									<h3>John <span>Deshmukh</span></h3>
									<h4>Dental Specialist</h4>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Our Doctors  -->
		
		
		
		<!-- Start Our Partners  -->
		<div class="parnter_area">
			<div class="container">
				<div class="our_partners_area">
					<div class="our_partner">
						<h1>Our partner</h1>
						<img src="images/our_doc_heart.png" alt="our doctors" />
						<div class="doctor_carousel partner_carousel">
							<div class="items">
								<img src="images/partner1.png" alt="doctor" />
								<div class="doc_overly">
								    <p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemenaugue. Aliquam vitae enim risus, at laoreet metus.elemen</p>
									<h3>Genelia Joshi</h3>
									<h4>Dental Specialist</h4>
								</div>
							</div>
							<div class="items">
								<img src="images/doctor6.png" alt="doctor" />
								<div class="doc_overly">
								    <p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemenaugue. Aliquam vitae enim risus, at laoreet metus.elemen</p>
									<h3>Genelia Joshi</h3>
									<h4>Dental Specialist</h4>
								</div>
							</div>
							<div class="items">
								<img src="images/partner5.png" alt="doctor" />
								<div class="doc_overly">
								    <p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemenaugue. Aliquam vitae enim risus, at laoreet metus.elemen</p>
									<h3>Genelia Joshi</h3>
									<h4>Dental Specialist</h4>
								</div>
							</div>
							<div class="items">
								<img src="images/partner2.png" alt="doctor" />
								<div class="doc_overly">
								    <p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemenaugue. Aliquam vitae enim risus, at laoreet metus.elemen</p>
									<h3>Genelia Joshi</h3>
									<h4>Dental Specialist</h4>
								</div>
							</div>
							<div class="items">
								<img src="images/partner3.png" alt="doctor" />
								<div class="doc_overly">
								    <p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemenaugue. Aliquam vitae enim risus, at laoreet metus.elemen</p>
									<h3>Genelia Joshi</h3>
									<h4>Dental Specialist</h4>
								</div>
							</div>
							<div class="items">
								<img src="images/partner4.png" alt="doctor" />
								<div class="doc_overly">
								    <p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemenaugue. Aliquam vitae enim risus, at laoreet metus.elemen</p>
									<h3>Genelia Joshi</h3>
									<h4>Dental Specialist</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Our Partners  -->
		
		<!--Start Counter Part -->
		<div class="counter_part">
			<div class="container">
				<div class="hospital_info">
					<div class="col-md-3 col-sm-3 paddingltnone">
						<div class="hospital_room">
							<div class="hospital_room_img">
								<img src="images/hospital_icon.png" alt="room" />
							</div>
							<div class="htext">
								<h2>100</h2>
								<h3>Hospital Rooms</h3>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 paddingltnone">
						<div class="doc_award">
							<div class="doc_award_img">
								<img src="images/doctor_award.png" alt="award" />
							</div>
							<div class="adtext">
								<h2>200</h2>
								<h3>Doctor Awwards</h3>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 paddingltnone">
						<div class="pro_doc">
							<div class="pro_dop">
								<img src="images/sto.png" alt="award" />
							</div>
							<div class="pdtext">
								<h2>500</h2>
								<h3>Proffessional Doctors</h3>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 paddingltnone">
						<div class="satisfied_patient">
							<div class="sts_imgp">
								<img src="images/facebook_like.png" alt="room" />
							</div>
							<div class="sptext">
								<h2>100</h2>
								<h3>Satisfied Patient</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--End Counter Part -->
		
		<!-- Start Book your appointment-->
		<div class="book_appointmen_bottom">
			<div class="container">
				<div class="booknow_btn">
					<div class="col-md-3 col-sm-3 paddingbothnone">
						<div class="your_app_img">
							<img src="images/booking.png" alt="booking img" />
						</div>
					</div>
					<div class="col-md-6 col-sm-5 paddingbothnone">
						<div class="booking_text_bottom">
							<h3>Book Your Appointment</h3>
							<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, ele ignissim sit</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-4 paddingltnone">
						<div class="booking_buttn_bottom">
							<a href="#">Book Appointment</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Book your appointment-->
		
		<?php
  
 			include(ROOT_SITE."/footer.php");


		?>
