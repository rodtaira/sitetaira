
		<!-- Start Footer -->
		<div class="footer_top_area">
			<div class="footer_top">
				<div class="container">
					<div class="footertop">
						<div class="col-md-3 col-sm-4">
							<div class="faddress">
								<img src="images/map_dir.png" alt="address" />
								<h3>Address</h3>
								<p>E104, Dharti 2, Near Silverstar, <br>Chennai, India</p>
							</div>
						</div>	
						<div class="col-md-3 col-sm-4">
							<div class="emcall">
								<img src="images/emcall.png" alt="address" />
								<h3>Emergency ( 24x7 )</h3>
								<p>Mobile: +91 9510357514 <br>Toll Free : 123 456 7890</p>
							</div>
						</div>	
						<div class="col-md-3 col-sm-4">
							<div class="emailus">
								<img src="images/mess.png" alt="address" />
								<h3>Email Us</h3>
								<p>info@medicare.com  & <br>info@medicarecenter.com</p>
							</div>
						</div>	
						<div class="col-md-3 col-sm-3 display-none">
							<div class="emailus">
								<img src="images/clock.png" alt="address" />
								<h3>Working Hours</h3>
								<p>Mon to Sat  <span>9 AM to 11 PM </span><br> 
								Sunday <span>10 AM to 6 PM </span></p>
							</div>
						</div>	
					</div>
				</div>
			</div>
			
			<div class="footer_middle">
				<div class="container">
					<div class="newsletter_part">
						<div class="col-md-4 col-sm-5 paddingltnone">
							<div class="news_letter">
								<h4>Subscribe Newsletter</h4>	
								<input type="email" id="nwsemail" placeholder="Email ID"/>
								<input type="submit" id="nwssubmit" value="GO" />
							</div>
						</div>	
						<div class="col-md-8 col-sm-7">
							<div class="medicare">
								<h3>About Medicare</h3>
								<p>Pellentesque turpis felis, dignissim sit amet sagittis ac, elemenmet sagittis ac, elemen elemeet sagittis ac, augugue. Aliquam vitae enim risus, at laoreet metus. </p>
							</div>
						</div>	
					</div>
				</div>
			</div>

			<div class="footer_menu_area">
				<div class="container">
					<div class="footer_menu_in">
						<div class="col-md-10 col-sm-10">
							<div class="footer_menu">
								<nav>
								    <ul>                                
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="doctors.html">Doctors</a></li>
                                        <li><a href="service.html">Services</a></li>
                                        <li><a href="gallery.html">Gallery</a></li>
                                        <li><a href="elements.html">Element</a></li>
                                        <li><a href="faqs.html">Term& condition</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                    </ul>
								</nav>
							</div>					
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="back_top">
								<a href="#"><img src="images/backtop.png" alt="backtop" /></a>
							</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer -->
		
		
		<!-- Start Footer Bottom -->
		<div class="footer_bottom">
			<div class="container">
				<div class="footer_copyright">
					<div class="col-md-8 col-sm-8">
						<div class="copy_text">
							<p>&copy; Copyright 2015. All Rights Reserved by <a href="#">Medicare</a></p>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Bottom -->
		
		<!-- JS Files -->
		<script src="js/vendor/jquery-1.11.2.min.js"></script>
		<!-- Google Maps API -->
        <script src="https://maps.googleapis.com/maps/api/js"></script>
		<!-- Bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.slicknav.min.js"></script>
		<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
		<script src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<!-- Bootstrap DatePicker-->
		<script src="js/bootstrap-datepicker.js"></script>
		<!-- Owl Carousel-->
		<script src="js/owl.carousel.min.js"></script>
		<!-- Mixitup -->
		<script src="js/jquery.mixitup.js"></script>
		<!-- Prity Photo -->
		<script src="js/jquery.prettyPhoto.js"></script>
		<!-- Accourdion -->
		<script src="js/smk-accordion.js"></script>
		<!-- Easy Responsive Tab -->
		<script src="js/easyResponsiveTabs.js"></script>

		<!--Opacity & Other IE fix for older browser-->
		<!--[if lte IE 8]>
			<script type="text/javascript" 	src="js/ie-opacity-polyfill.js"></script>
		<![endif]-->
		<script src="js/main.js"></script>
  </body>
</html>