/*
 * ==========================================================
 
 * ARQUIVO DE MONTAGENS DE OBJETOS 
 * As ações de clicks, changes, hover, focus, blur estao no configuracoes.js com eventos .live() 
 
 
 * Recebe parametro do endereco absoluto do TEMA escolhido
 * http://host/themes/[tema]...
 * ==========================================================
*/
var scripts = $('script');
for (i=0;i<scripts.length;i++){
   if(scripts[i].src.indexOf('widgets.js?') !== -1) { // Procura o parametro no script desjado. // No caso este aqui mesmo
	  var query = scripts[i].src.substr(scripts[i].src.indexOf('?')+1);
	  DIR_THEME = query;
   }
}
if(!DIR_THEME){
	alert("JavaScript sem referência HTTP_HOST");
	}
																											/* PENSAR NISSO -OBSERVACAO *********
																											/* PRECISO DA REFERENCIA DO URL_SITE
																											*  Nos Widgets Form Search e WMapa
																											*	estou usando um gato dir_tema/../../pages/etc etc*/

	
	
	


	
$(document).ready(function(){
	
	var dir_tema = DIR_THEME; /* REDECLARACAO da variável
							   * Ainda nao entendi o que aconteceu. 
							   * Em algumas paginas (tab_eventos_lista.php) o DIR_THEME perdeu a referencia.
							   * Tava vindo zero (0). Mas no document ready aparecia.
							   * Entao eu redeclarei na variavel dir_tema. Aí funcionou
							   */ 



/*
 * VALIDADE FORM
 * ===================================================
 */
if($("form.ajax").length > 0)
{
	$("form.ajax").each(function () 
	{
		$(this).validate();
		$(this).removeClass("ajax");
		$(this).addClass("hasAjax");
	});
}
			

/*
 * BXSlider
 * ===================================================
 * Documentacao http://bxslider.com/
 */
	if($(".bxslider").length > 0)
	{
		$('body').append("<script src='"+ dir_tema +"/widget/bxslider/jquery.bxslider.min.js'></script>");
		$('body').append("<link href='"+ dir_tema +"/widget/bxslider/jquery.bxslider.css' rel='stylesheet' type='text/css'>");
		
		$(".bxslider").each(function () 
		{
			/*Corrigido*/
			var speed = 1000;
			if($(this).attr('speed') != undefined){
				speed = $(this).attr('speed');
				}
			var pause = 5000;
			if($(this).attr('pause') != undefined){
				pause = $(this).attr('pause');
				}
			var min_slides = 1;
			if($(this).attr('min-slides') != undefined){
				min_slides = $(this).attr('min-slides');
				}
			var max_slides = 4;
			if($(this).attr('max-slides') != undefined){
				max_slides = $(this).attr('max-slides');
				}
			var slide_width = 170;
			if($(this).attr('slide-width') != undefined){
				slide_width = $(this).attr('slide-width');
				}
			var slide_height = 170;
			if($(this).attr('slide-height') != undefined){
				slide_height = $(this).attr('slide-height');
				}
			var slide_margin = 0;
			if($(this).attr('slide-margin') != undefined){
				slide_margin = $(this).attr('slide-margin');
				}
			var infinite_loop = false;
			if($(this).attr('infinite-loop') != undefined){
				infinite_loop = true;
				}
			var mode = 'horizontal';
			if($(this).attr('mode') != undefined){
				mode = $(this).attr('mode');
				}
			var auto = false;
			if($(this).attr('auto') != undefined){
				auto = true;
				}
			var pager = true;
			if($(this).attr('pager') != undefined){
				pager = false;
				}
			var controls = true;
			if($(this).attr('controls') != undefined){
				controls = false;
				}
			var adaptive_height = false;
			if($(this).attr('adaptive-height') != undefined){
				adaptive_height = true;
				}
			
			$( this ).bxSlider({
			  minSlides: min_slides,
			  maxSlides: max_slides,
			  slideWidth: slide_width,
			  slideHeight: slide_height,
			  slideMargin: slide_margin,
			  infiniteLoop: infinite_loop,
			  speed: speed,
			  pause: pause,
			  adaptiveHeight:	adaptive_height,
			  mode: mode,
			  auto: auto,
			  pager: pager,
			  controls:controls
			}); 
		
			$( this ).removeClass("bxslider");
		})
		
	}
	
		
/*
 * WThumbs
 * ===================================================
 */
if($(".wthumb").length > 0)
{
	 $('body').append("<link href='"+ dir_tema +"/widget/wthumb/wthumb.css' rel='stylesheet' type='text/css'>");
} 
$(".wthumb").each(function(){
	// Gera um numero randomico entre 1 e 10000 
		// Vai ser o ID da moldura  
		// OBS: o idRandom nao pode ser decimal (0.96896) senao a aplicacao nao funciona
		$(this).removeClass('wthumb');
		var classes = $(this).attr("class");
		var styles = $(this).attr("style");
		var src = $(this).attr("src");
		var id = $(this).attr("id");
		if(id){
			var idRandom = id; 
		}else{
			var idRandom = Math.floor((Math.random()*10000)+1); 
		}
		
		if($(this).attr("alt"))var legenda = "<span class='legenda'>"+$(this).attr("alt")+"</span>";
		else legenda= "";
		$(this).before("<div class='hasWthumb "+classes+"' id='"+idRandom+"' style='background-image:url("+src+"); "+styles+"'>"+legenda);
		/*  
			// solucao antiga - usando index() e eq() :: preservar para estudos futuros
			var n = $(".thumb").index($(this)); 
			$('.moldura').eq(n).append($(this));
	 */
		$('#'+idRandom).append($(this)); 
		$(this).remove();
});
	
	
	
/*
 * DataTable
 * ===================================================
 * Documentacao http://www.datatables.net/
 */
if($(".datatable").length > 0)
{
	$('body').append("<script src='"+ dir_tema +"/widget/datatable/jquery.datatables.js'></script>");
	$('body').append("<link href='"+ dir_tema +"/widget/datatable/datatables.css' rel='stylesheet' type='text/css'>");
	
}
$(".datatable").each(function () { // Com isso eu coloco a classe nolength e nofilter na tabela 


	if($(this).attr('sort'))
	{
		
		//DATATABLE SECO - SEM SORT SEM NADA
		if($(this).attr('sort')=="false")
		{
			$(this).dataTable({
				"bSort": false
			});
		}else{
			//AQUI COM SORT - PEGO TAMBEM A DIREÇÃO E COLUNA
			var direction = $(this).attr('sort');
			var coluna = $(this).attr('sort_column');
			
			$(this).dataTable(
			{
				"aaSorting": [[ coluna, direction ]],
			});
		}
		
	}else{
		$(this).dataTable();
	}
	
	
	$(this).removeClass("datatable");
	var classes = $(this).attr("class");
	
	
	$(this).parent().addClass(classes);
	$(this).removeAttr("class");
});


	
/*
 * DATEPICKER
 * ===================================================
 */		
if($(".datepicker").length > 0)
{

		$('body').append("<script src='"+ dir_tema +"/../../lib/jquery/ui/jquery-ui.custom.js'></script>");
		$('body').append("<script src='"+ dir_tema +"/../../lib/jquery/ui/jquery.ui.datepicker.js'></script>");
		$('body').append("<link href='"+ dir_tema +"/../../lib/jquery/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'>");
		
		$(".datepicker").each(function () {
			var formato = "dd/mm/yy";
			if($(this).attr('dateFormat') != undefined){
				formato = $(this).attr('dateFormat');
				}
			
			var before = "";
			if($(this).attr('beforeShowDay') != undefined){
				before = $(this).attr('beforeShowDay');
				}
			
			var numbermonth = 1;
			if($(this).attr('numberOfMonths') != undefined){
				numbermonth = $(this).attr('numberOfMonths');
				}
			
			var changeMonth = false;
			if($(this).attr('changeMonth') != undefined){
				changeMonth = $(this).attr('changeMonth');
				}
			
			var changeYear = false;
			if($(this).attr('changeYear') != undefined){
				changeYear = $(this).attr('changeYear');
				}
			
			var minDate = false;
			if($(this).attr('minDate') != undefined){
				minDate = $(this).attr('minDate');
				}

			$( this ).datepicker({
				dateFormat:formato,
				beforeShowDay:before,
				minDate: parseInt(minDate),
				numberOfMonths:parseInt(numbermonth),
				changeMonth: changeMonth,
      			changeYear: changeYear
			});
			
			$( this ).removeClass("datepicker");
		})
		
	};
	
	
	
/*
 * WPopup
 * ===================================================
 */
var loopWpopup = false;
$(".wpopup-play").each(function()
{
	$(this).addClass("hasWpopup");
	$(this).removeClass("wpopup-play");
	if(loopWpopup==false){
		$('body').append("<link href='"+ dir_tema +"/widget/wpopup/wpopup.css' rel='stylesheet' type='text/css'>");
		loopWpopup = true;
	}
});


/*
 * Selectable
 * ===================================================
 * SELECTABLE :: select.selectable (Simula opções de RadioButton)
 * Cria <div class='btn-group'> e cada filho representará um option do select
 * MONTANDO O SELECT (Multiple=false)
 * OBS: Essa funcao deve ser listada aqui, ANTES da funcao $(".btn-group.selectable").children().click()
 */
	$("select.selectable").each(function()
	{
		var name 		= 	$(this).attr("name");
		var style 		= 	$(this).attr("style");
		var filhos 		= 	$(this).children().length;
		var idRandom 	= 	Math.floor((Math.random()*10000)+1); 
		
		//Cria div btn-group
		$(this).before("<div class='btn-group selectable' id='"+idRandom+"' rel="+name+" style='"+style+"'>");
		
		for(i=0;i<filhos;i++)
		{
			var value		= $(this).children().eq(i).val();
			var texto		= $(this).children().eq(i).html();


			var active		= "";
			var selected	= $(this).children().eq(i).attr('selected');
			if ( selected == "selected") active  = "active";
			
			var disabled	= "";
			var status		= $(this).children().eq(i).attr('disabled');
			if ( status == "disabled" ) disabled  = "disabled";
			
			var classes	= "";
			var tclass		= $(this).children().eq(i).attr('class');
			if ( tclass != "undefined" ) classes  = tclass;
			
			var sequence_target	= "";  // Criação exclusiva para o fieldset_sequence
			if($(this).children().eq(i).attr('sequence-target')){
				var sequence_target	= "sequence-target='"+$(this).children().eq(i).attr('sequence-target')+"'";
			}
			
			var item     	= "<span class='btn "+active+" "+disabled+" "+classes+"' "+sequence_target+" rel='"+value+"'>"+texto+"</span>";
			
			$("#"+idRandom).append(item);
		}
		
		$(this).hide();
		$(this).removeClass("selectable");
	});
		
/*
 * Checable
 * ===================================================
 * CHECABLE :: btn-group.checkable (multiple="multiple")
 * click habilita e desabilita a classe active nos filhos (pode ser <span>, <div> ou <a>)
 */		
	$("select.checkable").each(function()
	{
			var name 		= 	$(this).attr("name");
			var filhos 		= 	$(this).children().length;
			var idRandom 	= 	Math.floor((Math.random()*10000)+1); 
			
			$(this).attr('multiple','multiple');
			
			//Cria div btn-group
			$(this).before("<div class='btn-group checkable' id='"+idRandom+"' rel="+name+">");

			for(i=0;i<filhos;i++)
			{
				var value		= $(this).children().eq(i).val();
				var texto		= $(this).children().eq(i).html();
				
				var active		= "";
				var selected	= $(this).children().eq(i).attr('selected');
				if ( selected == "selected" ) active  = "active";
				
				var disabled	= "";
				var disabled	= $(this).children().eq(i).attr('disabled');
				if ( disabled == "disabled" ) disabled  = "disabled";
				
				var classes	= "";
				var tclass		= $(this).children().eq(i).attr('class');
				if ( tclass != "undefined" ) classes  = tclass;
				
				// Criação exclusiva para o fieldset_sequence
				var sequence_target	= "";  
				if($(this).children().eq(i).attr('sequence-target')){
					var sequence_target	= "sequence-target='"+$(this).children().eq(i).attr('sequence-target')+"'";
				}
				
				// Criação exclusiva para o hasWtoggle
				var wtoggle_target	= "";  
				var selfclick		= ""; 
				if($(this).children().eq(i).attr('wtoggle-target')){
					var wtoggle_target	= "wtoggle-target='"+$(this).children().eq(i).attr('wtoggle-target')+"'";
				}
				if($(this).children().eq(i).attr('selfclick')){
					var selfclick	= "selfclick='"+$(this).children().eq(i).attr('selfclick')+"'";
				}
				
				
				var item     	= "<span class='btn "+active+" "+disabled+" "+classes+"' "+sequence_target+" "+wtoggle_target+" "+selfclick+" rel='"+value+"'>"+texto+"</span>";
				
				$("#"+idRandom).append(item);
			}
			$(this).hide();
			$(this).removeClass("checkable");
	});
		
	
/*
 * selectToFrom
 * ===================================================
 * TO/FROM (Listas) :: select.selectToFrom
 * Faz correlações entre selects buscando o parametro "to"
 */	
	$(".selectToFrom.destino option").attr("selected","selected");
			
	
/*
 * Skinned
 * ===================================================
 * Transforma o bixao
 * A acao on('change',function()) esta em configuracoes
 * A partir de um SELECT, monta uma estrutura de <div class='skinned-select'> no fim append select 
 * e ao final REMOVECLASS skinned do select
 */
	$("select.skinned").each(function()
	{
		var state  = $(this).attr("disabled");
		var valor_inicial = "<div class='skinned-select-text'>"+  $(this).find("option:selected").html() + "</div>";
		var width = $(this).width();
		var styles = "";
		
		if($(this).attr('style')!="undefined")
		{
			var styles = $(this).attr('style');
			$(this).attr('style','');//1-limpa
			if(width>0){
				$(this).attr('style','width:'+width	+'px');//2-insere so o tamanho
				}
		}
		var idRandom 	= 	Math.floor((Math.random()*10000)+1); 
		//Cria div
		$(this).before("<span class='skinned-select "+state+"' id='"+idRandom+"' style='"+styles+"'>");
		//Aplica valor e Select
		$("#"+idRandom).append(valor_inicial, $(this));
		$(this).removeClass("skinned");
		$(this).addClass("hasSkinned");
		
		if($(this).hasClass('black')){
			$("#"+idRandom).addClass("black");
			}
		
		if($(this).hasClass('mini')){
			$("#"+idRandom).addClass("mini");
			}
	});
		
/*
 * Search
 * ===================================================
 * Obs: Existe uma exceção para esse form. O submit dele é normal. Diferente do item X.X SUBMIT PADRAO :: linha 110?
 */
/* $("input.search").each(function(){
		var idRandom = Math.floor((Math.random()*10000)+1); 
			$(this).before("<form action='"+dir_tema+"/../../@gerenciador/pesquisa' class='search' id='"+idRandom+"'>");
			
			$(this).attr('name','q');
			//input no form
			$(this).removeClass('search');
			$("#"+idRandom).append($(this));
	});*/


/*
 * WTAGINPUT - Cria tags como Facebook (SELECT MULTIPLE)
 ************** IMPORTANTE ************************
 * Deve vir ANTES DO AUTOCOMPLETE porque?
 * Em alguns tagInputs eu posso querer usar o autocomplete tambem entao ele deve primeiro montar o wtaginput
 * e depois passar pela montagem do autocomplete para dar certo
 * ===================================================
 * Transforma o bixao
 */
	if($(".wtaginput").length > 0)
	{
		$('body').append("<link href='"+ dir_tema +"/widget/wtaginput/wtaginput.css' rel='stylesheet' type='text/css'>");
	}
	
	
	$(".wtaginput").each(function()
	{
		var objeto = $(this);
		objeto.removeClass('wtaginput');
		var styles = "";
		var idRandomEst 	= 	Math.floor((Math.random()*10000)+1); 
		var name = objeto.attr('name');
		var classes = objeto.attr('class');
		
		if(objeto.attr('style')!="undefined")
		{
			var styles = objeto.attr('style');
			objeto.attr('style',''); //reseta stilos
		}
		
		//1 - MONTA A ESTRUTURA e INSERE NO BODY
		var estrutura = $("<div>").addClass("hasWtaginput "+classes).attr("id",idRandomEst).attr("style",styles).html("<div class='tagarea'></div><div class='inputarea'></div>"); 
		objeto.after(estrutura);
		
		
		//2 - DEFINE O INPUT
		//Se foi setado AUTOCOMPLETE Mede a largura X do div hasWtaginput - que acabei de criar e aplica no input
		var default_id = "";
		if(objeto.attr('default-id')!=undefined){
			default_id = objeto.attr('default-id');
		}
		var idRandomInput 	= 	Math.floor((Math.random()*10000)+1); 
		var largura = objeto.width()-10;
		var input = $('<input>').attr('type','text').attr('name','campo').attr('id',idRandomInput).attr('style','width:'+largura+'px');
		if(objeto.hasClass('wautocomplete'))input = input.addClass("wautocomplete").attr('tabela',objeto.attr('tabela')).attr('referencia',objeto.attr('referencia')).attr('taginput_pai_id',idRandomEst).attr('default-id',default_id);//add parametros
		//Aplica o input
		$('#'+idRandomEst+' .inputarea').append(input);
		
		
		objeto.removeAttr("class");
		$("#"+idRandomEst).append(objeto);
		objeto.hide();
		objeto.find('option').each(function(){
			var opt = $(this);
			var tag = $('<span>').addClass('tag btn').html("<span class='tagValue' val='"+opt.val()+"'>"+opt.html()+"</span> <span class='tagClose'></span>");
			$("#"+idRandomEst+" .tagarea").append(tag);
			})

	});
	
	
	/*
 * WCountDown
 * ===================================================
 */
 $('.wcontador .relogio').each(function()
 {
		var contador = $(this);
		var count_style = contador.attr('count-style');
		var type_action = contador.attr('type-action');
		var action = contador.attr('action');
		var params = contador.attr('params');
		
		switch(count_style)
		{
			case "image":
				contador.countdown({
				image		: 	contador.attr('image'),
				digitWidth	: 	parseInt(contador.attr('digitWidth')),
				digitHeight	: 	parseInt(contador.attr('digitHeight')),
				format		: 	contador.attr('format'),
				status		: 	contador.attr('status'),
				startTime	: 	contador.attr('startTime'),
				timerEnd	: 	function(){ 
									switch(type_action)
									{
											case "ajax":
											/*EXECUTA TRANSACAO*/
											var loader = new Loader();
											$.post(dir_tema + "/../../pages/transacao/transacao.php?form_action=" + action, params, function(retorno,status,xhr){
													if(typeof(retorno)=="object"){
														var feedback = new Feedback();
														feedback.setAction(retorno.action);
														feedback.setTipo(retorno.tipo);
														feedback.setTempo(retorno.tempo);
														feedback.setTitulo(retorno.titulo);
														feedback.setMensagem(retorno.mensagem);
														feedback.setURL(retorno.url);
														feedback.start();
													}else{ // number ou string
														var feedback = new Feedback();
														feedback.setTipo("alert");
														feedback.setTitulo("Mensagem do sistema");
														feedback.setMensagem(retorno);
														feedback.start();
													}
												},"json")
												.always(function() { 
													loader.esconde();
												 });
											break;
											case "button_url":
											contador.parent().html("<span class='btn btn-small btn-primary' onclick='"+action+"'>"+params+"</span>");
											break;
											
											case "location":
											window.location.href=params;
											break;
									}
								}
				});
			break;
			case "text":
				contador.html(contador.attr('startTime'));
				var Timer = setInterval(startCountdown,1000);
				var parts = contador.attr('startTime').split(":");
				var h = parts[0];
				var m = parts[1];
				var s = parts[2];
				
				function startCountdown(){
					if((s - 1) >= 0)
					   {
						   s = s - 1;
						   atualizaTimer(h,m,s);
					   }
					   if((s - 1) == -1) // Segundo ZERO
					   {
							   if(h==0 && m==0 && s==0){
									timerFim(type_action);
									return false;
							   }else{
									   if((m-1)>=0)
									   {
										   atualizaTimer(h,m,s);
										   m = m-1;
									   }
									   
									   if((m-1)==-1)//Minuto ZERO
									   {
										   atualizaTimer(h,m,s);
										   if(h>0){
											   m=59;
											   h = h-1;
										   }
									   }
									   s = 59;
							   }
					   }
				}
				
				function atualizaTimer(hora,minuto,segundo){
					if(String(hora).length==1) hora="0"+hora;
					if(String(minuto).length==1) minuto="0"+minuto;
					if(String(segundo).length==1) segundo="0"+segundo;
					contador.html(hora+":"+minuto+":"+segundo);
					}
					
				function timerFim(type_action)
				{ 
						clearInterval(Timer);
						switch(type_action)
						{
								case "ajax":
								/*EXECUTA TRANSACAO*/
								var loader = new Loader();
								$.post(dir_tema + "/../../pages/transacao/transacao.php?form_action=" + action, params, function(retorno,status,xhr){
										if(typeof(retorno)=="object"){
											var feedback = new Feedback();
											feedback.setTipo(retorno.tipo);
											feedback.setTempo(retorno.tempo);
											feedback.setTitulo(retorno.titulo);
											feedback.setMensagem(retorno.mensagem);
											feedback.setURL(retorno.url);
											feedback.setAction(retorno.action);
											feedback.start();
										}else{ // number ou string
											var feedback = new Feedback();
											feedback.setTipo("alert");
											feedback.setTitulo("Mensagem do sistema");
											feedback.setMensagem(retorno);
											feedback.start();
										}
									},"json")
									.always(function() { 
										loader.esconde();
									 });
								break;
								
								case "button_url":
								contador.parent().html("<span class='btn btn-small btn-primary' onclick='"+action+"'>"+params+"</span>");
								break;
								
								case "location":
								window.location.href="'"+params+"'";
								break;
								
						}
				}
			break;
		}
});
	
	
	
	
/*
 * WAUTOCOMPLETE - Meu filho mais querido
 * ===================================================
 * Cria objeto de busca com a seguinte estrutura:
 * <div class="hasWautocomplete" id="2507">
 * 	 <input type="text" id="cidade" name="cidade" tabela="cidade" value="">
 * 	 <ul class="wautocomplete-list"></ul>
 * 	 <input type="hidden" name="cidade_id" value="">
 * </div>
 * Documentação:
 * o "name" do campo ganhara o complemento "_id" e sera inserido no hidden // ficando: <input type="hidden" name="cidade[_id]" > 
 * Na tabela ja tenho que ter esse espaço do campo "coluna_id"
 */
 	if($("input.wautocomplete").length > 0)
	{
		$('body').append("<link href='"+dir_tema+"/widget/wautocomplete/wautocomplete.css' rel='stylesheet' type='text/css' >");
	}
	
	$("input.wautocomplete").each(function()
	{
			$(this).removeClass('wautocomplete');
			var idRandom2 = Math.floor((Math.random()*10000)+1)+"a"; 
		
			var default_id = "";
			if($(this).attr('default-id')!=undefined){
				default_id = $(this).attr('default-id');
			}
			
			var inputName = $(this).attr('name');
			var ulList = "<ul class='wautocomplete-list'></ul>";
			var inputHidden = "<input type='hidden' name='"+inputName+"_id' value='"+default_id+"'>";
		
			$(this).before("<div class='hasWautocomplete' id='"+idRandom2+"'>");
			
			$(this).attr('autocomplete','off');

			$("#"+idRandom2).append($(this));
			$("#"+idRandom2).append(ulList);
			$("#"+idRandom2).append(inputHidden);
			//$("#"+idRandom).append("<link href='"+dir_tema+"/widget/wautocomplete/wautocomplete.css' rel='stylesheet' type='text/css' >" );
			
	});



	

/*
 * Wmapa - Consulta as reservas de acordo com a sessao 
 * ===================================================
 */	
	if($(".wmapa").length > 0)
	{
		$('body').append("<link href='"+ dir_tema +"/widget/wmapa/wmapa.css' rel='stylesheet' type='text/css'>");
		$(".wmapa").hide();
		
		$(".wmapa").each(function () 
		{
				var idRandom = Math.floor((Math.random()*10000)+1); 
				$(this).attr('id',idRandom);
				$(this).removeClass("wmapa");
				$(this).addClass("hasWmapa");

				var sessao_id = $('#'+idRandom).attr('sessao-id');
				var pedido_id = $('#'+idRandom).attr('pedido-id');
				var carrinho_id = $('#'+idRandom).attr('carrinho-id');
				var setor_id = $('#'+idRandom).attr('setor-id');
				
				var ultimo_indice_consultado = $('#'+idRandom +' tr:last-child td:last-child').attr('id');
				
				$('#'+idRandom +' td').each(function() 
				{
						var objeto = $(this);
						var mapa_id = $(this).attr('id');
						var parametrosEnvio = "&mapa_id="+mapa_id + "&sessao_id="+sessao_id + "&pedido_id="+ pedido_id + "&carrinho_id="+ carrinho_id + "&setor_id="+ setor_id;
						$.post(dir_tema+"/../../pages/transacao/transacao.php?form_action=consultar_reserva", parametrosEnvio, function(retorno,statusEnvio,xhrEnvio){
							//alert(retorno);
							if(retorno.mensagem=="reservada_outro"){
									objeto.addClass("disabled");
								}
							if(retorno.mensagem=="selecionada_voce"){
									objeto.addClass("selected");
								}
							
							//RESOLVE PRELOADER HORIZONTAL E ICONE PRELOADER SOME AO FINAL
							/*if(retorno.param1==ultimo_indice_consultado){
									$('#'+idRandom).fadeIn(200,function(){$(this).parent().removeClass("preloader-mapa");});
								}*/
							if(retorno.param2>"5")
							{
								$("#preloader-horizontal").show();
								if(retorno.param2<"75"){
									var porcento = retorno.param2-25;
									$("#preloader-horizontal").css('width',porcento+'%');
								}else{
									$.post(dir_tema+"/../../pages/transacao/transacao.php?form_action=zerar_contagem_reserva", "&setor_id="+ setor_id, function(retorno,statusEnvio,xhrEnvio){
											//alert("OK");
									});
									$("#preloader-horizontal").animate({width:"100%",opacity:"0.3"},function(){ 
											$('#'+idRandom).fadeIn(200,function(){$(this).parent().removeClass("preloader-mapa");});
											$(this).remove();
									 });
								}
							}
							
						},"json")
						
				});
		});
	}


/*
 * WToggle
 * ===================================================
 * Generico para show/hide objetos
 * Criei inicialmente para operar com os tooltips (Baloezinhos de legenda) 
 */
	$(".wtoggle").each(function()
	{
		$(this).addClass("hasWtoggle");
		$(this).removeClass("wtoggle");
	});
	
	
	
/*
 * svg_area - MAPA
 * ===================================================	
*/
$(".svg_area").each(function () 
{
	var svg_area 	= $(this);
	var sessao_id 	= svg_area.attr('sessao-id');
	var setor_id 	= svg_area.attr('setor-id');
	var pedido_id 	= svg_area.attr('pedido-id');
	var usuario_id 	= svg_area.attr('usuario-id');
	var date 		= svg_area.attr('date');
	
	var parametros = "&usuario_id="+usuario_id+"&sessao_id="+sessao_id+"&setor_id="+setor_id+"&pedido_id="+pedido_id+"&date="+date;
	


	/*22/08 - Deixa eu explicar a história desse estilo aqui
	Os SVG só reagem ao CSS se caso o style esteja dentro da tag <svg>, entao de nada adiantaria eu montar um estilo pra essas merda no corpo da pagina ou em css separado.
	Entao como eu estou fazendo algumas transformacoes nos "rects", aproveito para enfiar o <style> dentro do <svg> aqui.
	*/
	var estilo = "<style>.selected{fill:#C00;stroke:#F00;stroke-width:0px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:0;}.enabled{fill:#77b4c7;}.disabled,.fake{fill:#000000;}.selected:hover,.enabled:hover,.fake:hover{opacity: 0.7;cursor:pointer;}</style>";
	
	$("svg").prepend(estilo);
	
	
	
				
	$.post(dir_tema+"/../../pages/transacao/transacao.php?form_action=consulta_reservas", parametros, function(retorno,status,xhr)
	{
			//alert(retorno);
			
			for(i=0;i<retorno.length;i++)
			{
				var id = (retorno[i].id).toString();
				var objeto = $("#"+id);
				
				
				if(objeto.length>0)
				{
					switch(retorno[i].status)
					{
						case "reservada":
						objeto.attr('class',"disabled");
						objeto.attr('onclick',"SVG_mostra_reserva(this);");
						break;
						case "fake":
						objeto.attr('class',"fake");
						objeto.attr('onclick',"SVG_aplicar_reserva(this);");
						break;
						case "disponivel":
						objeto.attr('class',"enabled");
						objeto.attr('onclick',"SVG_aplicar_reserva(this);");
						break;
						case "selecionada_voce":
						objeto.attr('class',"selected");
						objeto.attr('onclick',"SVG_aplicar_reserva(this);");
						break;
					}
					objeto.attr('title',retorno[i].title);
				}
			}
	},"json")
		
})	
	

	
																						
});