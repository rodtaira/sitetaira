<?php
/*
Criado em 05/07/2013
Widget que busca informacoes no Showcommerce
Por enquanto estou passando todos os parametros por GET, Nada via POST ainda.

// PARAMETROS DE CONSTRUCAO
* feed  		-  Nome da Funcao		(Padrao "")
* parametros    -  Array de parametros  (Padrao array())
* limit  		-  Limite de consultas	(Padrao false ou "")
* token  		-  ID do cliente		(Padrao "")
* type  		-  Tipo de retorno		(Padrao "json")

//exemplo de uso (Coberturas):
$wid = new WCurl();
$wid->feed 	 	 = 'cobertura.Lista';
$wid->token 	 = $_SESSION['cliente_id'];
$wid->parametros = array(
					'id'=>$_GET['id']
					);
$stdClass = $wid->start();

$Cobertura = $stdClass[0];
$arr_fotos = $stdClass['fotos'];



*** 12/08/2013 ****
====================================================================================================
               I  M  P  O  R  T  A  N  T  E  (SERIALIZE para arrays)
====================================================================================================
Tive um problema ao fazer uma requisicao utilizando um array dentro do parametro.
A requisicao foi 'transacao.novo_pedido', que informa a quantidade de produtos dentro de um array...
segue desenho: $qtd = array('IDPRODUTO1'=>1,
							'IDPRODUTO2'=>3,
							'IDPRODUTO3'=>1
							)
Quanto acontece isso eu tenho que usar a funcao serialize($qtd);
E la no feed do showcommerce eu tenho que usar unserialize($qtd);
*/

class ShowCommerceRequest
{

    public function __construct($SCfeed="", $parametros=array(), $SCmethod="POST", $SClimit = false , $SCtype="json")
    {
			$this->SCfeed = $SCfeed;
			$this->SCtype = $SCtype;
			$this->SClimit = $SClimit;
			$this->SCmethod = $SCmethod;

			//Define a URL // prioridade para a url do config.php
			if(defined('URL_FEEDS')){
				$this->SCurl = URL_FEEDS;
			}else{
				$this->SCurl = 'https://www.showcommerce.com.br/pages/feed2015/';
			}

			//Define o TOKEN // prioridade para o CLIENTE_ID do config.php
			if(defined('CLIENTE_ID'))
			{
				$this->SCtoken = CLIENTE_ID;
			}else {
				@session_start();
				$this->SCtoken = $_SESSION['cliente_id'];
			}

			//Cria Array parametros e atribui ao parametro "parametro"
			$parametros['SCfeed'] = $this->SCfeed;
			$parametros['SCtype'] = $this->SCtype;
			$parametros['SCfeed'] = $this->SCfeed;
			$parametros['SClimit'] = $this->SClimit;
			$parametros['SCtoken'] = $this->SCtoken;
			$parametros['ip'] 	   = $_SERVER['REMOTE_ADDR'];
			$parametros['so'] 	   = $_SERVER['HTTP_USER_AGENT'];

			//Serialize para arrays //cláusula IMPORTANTE de 12/08/2013
			foreach($parametros AS $param=>$value){
				if(is_array($value))$value = serialize($value);
				$parametrosRevisado[$param] = $value;
				}

			$this->SCparametros = $parametrosRevisado;
    }

	public function start()
	{
			//Checa os PARAMETROS fundamentais //TOKEN //FEED
			if($this->SCfeed==false || $this->SCtoken==false)
			{
				$feedback = '<strong>Erro na integração do sistema!</strong><br />';
				$feedback .= 'Faltam os seguintes parâmetros:';

				$feedback .= '<ul>';
				if($this->SCfeed==false) $feedback .= '<li>Feed</li>';
				if($this->SCtoken==false) $feedback .= '<li>Token de identificação do Cliente</li>';
				$feedback .= '</ul>';
				echo $feedback;
				//exit;
			}


			/** CHAMA ACAO CURL **/
			$ch = curl_init();
			if($this->SCmethod=="POST")
			{
				curl_setopt($ch,CURLOPT_URL, $this->SCurl);
				curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $this->SCparametros);
				//echo $this->parametros.'&feed='.$this->feed.'&token='.$this->token.'&type='.$this->type;
			}
			if($this->SCmethod=="GET")
			{
				curl_setopt($ch, CURLOPT_URL, serialize($this->SCparametros));
			}
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //PERMITE PUXAR USANDO LOCALHOST
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // êêêsse é o bixao. Faz com que eu possa armazenar o resultado do echo em $retorno = curl_exec($ch);
			curl_setopt($ch, CURLOPT_REFERER, $_SERVER["HTTP_HOST"]); // Obrigatori setar pois eu vou cobrar la no arquivo do showcommerce// So vou habilitar para os sites cadastrados como clientes
			$output = curl_exec($ch);
			//echo gettype ($output);// string
			$json = json_decode($output);
			curl_close($ch);


			//CASO SEJA SO UM TESTE (Exibe retorno na pagina)
			if(isset($this->dump) && $this->dump==true){
				print_r($json);
				echo "<br /><br /><br /><br />";
				return false;
				}


			switch(gettype($json))
			{
				//RETORNO E TRATAMENTO JSON
				case "object":
					//Transforma em objeto	 // ex de retorno em objeto http://www.showcommerce.com.br/pages/feed/?feed=cobertura.Lista&token=513692353381386792163&id=1372950754550347540
					$stdClass = get_object_vars($json);
					return $stdClass;

				break;
				case "array"://Retorna so o array mesmo  // ex de retorno em Array http://www.showcommerce.com.br/pages/feed/?feed=banner.Lista&token=513692353381386792163
				case "bool":
				case "string":
					return $json;
				break;
				case "NULL":
					return "Sua pesquisa resultou num conjunto vazio.";
				break;
				default:
					echo gettype ($json);
					echo "<strong>Erro na integração!</strong><br />
					O Objeto retornado não está de acordo com a especificação da requisição.";
					//echo gettype($json);
					//exit;
				break;
			}

    }
}
?>
