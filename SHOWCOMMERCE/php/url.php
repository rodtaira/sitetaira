<?php
// Recebe a URL digitada


$URL = $_SERVER['REQUEST_URI'];

//RETORNA O HOST_EXTRA
$LINHA_COMPLETA		= __FILE__;
$CONF_LOCAL_ROOT   	= $_SERVER['DOCUMENT_ROOT'];
$CONF_LOCAL_URL   	= $_SERVER['HTTP_HOST'];
$HOST_EXTRA = str_replace("\\","/",$LINHA_COMPLETA);
$HOST_EXTRA = str_replace($CONF_LOCAL_ROOT,"",$HOST_EXTRA);
$HOST_EXTRA = str_replace($CONF_LOCAL_URL,"",$HOST_EXTRA);
$HOST_EXTRA = str_replace("/SHOWCOMMERCE/php/url.php","",$HOST_EXTRA);
$HOST_EXTRA_BARRAS = str_replace("/","\\/",$HOST_EXTRA); //Acrescenta as barras invertidas

//CONSTANTE "HOST_EXTRA"
define('HOST_EXTRA'	 ,$HOST_EXTRA); //está no url.php
$URL = str_replace(HOST_EXTRA,"", $URL);
$URL = trim($URL , "/");

//LIMPA CACHE
//$MATCHE_LIMPA = "/^{$HOST_EXTRA_BARRAS}\/limpa|{$HOST_EXTRA_BARRAS}\/logout$/i";
if(preg_match_all("/^limpa$/i", $URL, $matches,PREG_SET_ORDER))
{
	include("limpa.php");
	exit;
}



include ("config.php");

if($URL)
{
			
			#------------------------
			# NAVEGACAO SITE
			#------------------------
			if(preg_match_all("/^Empresa$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/empresa.php");
				exit;
			}
			if(preg_match_all("/^Orcamento$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/orcamento.php");
				exit;
			}
			if(preg_match_all("/^admin$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				header("Location: http://showcommerce.com.br");
				exit;
			}
			if(preg_match_all("/^Optout\/(.*)$/i", $URL, $matches,PREG_SET_ORDER))
			{
				//print_r($matches);
				$USUARIO_ID = $matches[0][1];
				include(ROOT_PAGES . "/optout.php");
				exit;
			}
			if(preg_match_all("/^login|cadastro$/i", $URL, $matches,PREG_SET_ORDER))
			{
				include(ROOT_PAGES . "/cadastro.php");
				exit;
			}
			if(preg_match_all("/^Contato|FaleConosco$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/contato.php");
				exit;
			}
			
			if(preg_match_all("/^Blog$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/blog.php");
				exit;
			}
			if(preg_match_all("/^Servicos$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/servicos.php");
				exit;
			}
			if(preg_match_all("/^DuvidasFrequentes|FAQ$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/faq.php");
				exit;
			}
			
			
			
			
			
			
			
			
			
			
			
			///////////////////////////////////////////////////////////////////////////
			
			//DAQUI PRA BAIXO NAO ESTA USANDO, MAS PODE SER USADO NO FUTURO
			
			////////////////////////////////////////////////////////////////////////////
			
			
			if(preg_match_all("/^Busca/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				$STR = parse_url($URL, PHP_URL_QUERY); //Retorna tudo o que vier depois do Interrogacao: "site/loja?"
				parse_str($STR,$OUTPUT); //Converte str como se ela tivesse sido passada via URL e define o valor das variáveis.  Ex: ?offset=20&categoria=Saia
				include(ROOT_PAGES . "/busca.php");
				exit;
			}
			if(preg_match_all("/^EsqueciMinhaSenha$/i", $URL, $matches,PREG_SET_ORDER))
			{
				include(ROOT_PAGES . "/esqueci_senha.php");
				exit;
			}
			
			
			
			#------------------------
			# E-COMMERCE
			#------------------------
			if(preg_match_all("/^Carrinho$/i", $URL, $matches,PREG_SET_ORDER))
			{
				include(ROOT_PAGES . "/ecommerce/carrinho.php");
				exit;
			}
			if(preg_match_all("/^Checkout$/i", $URL, $matches,PREG_SET_ORDER))
			{
				include(ROOT_PAGES . "/ecommerce/checkout.php");
				exit;
			}
			if(preg_match_all("/^Checkout\/(.*)$/i", $URL, $matches,PREG_SET_ORDER))
			{
				$PEDIDO_ID = $matches[0][1];
				include(ROOT_PAGES . "/ecommerce/checkout.php");
				exit;
			}
			if(preg_match_all("/^retorno\/facebook$/i", $URL, $matches,PREG_SET_ORDER))
			{
				include(ROOT_PAGES . "/site/retornoLoginFacebook.php");
				exit;
			}
			if(preg_match_all("/^retorno\/(pagseguro|passepague)\/(.*)$/i", $URL, $matches,PREG_SET_ORDER))
			{
				//print_r($matches);
				$MEIO = $matches[0][1];
				$TID = $matches[0][2];
				include(ROOT_PAGES . "/ecommerce/retornoPagamento.php");
				exit;
			}
			if(preg_match_all("/^retorno\/(.*)$/i", $URL, $matches,PREG_SET_ORDER))
			{
				//print_r($matches);
				$TID = $matches[0][1];
				include(ROOT_PAGES . "/ecommerce/retornoPagamento.php");
				exit;
			}


			


			# COBERTURAS ==================================================================================================
			if(preg_match_all("/^Coberturas$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/site/coberturas.php");
				exit;
			}
			if(preg_match_all("/Coberturas\/([0-9]{4})\/([0-9]{2})\/(.*)?/i", $URL, $matches,PREG_SET_ORDER)) // Macete: Não usar o "^" no inicio, para nao configurar que é no inicio da string
			{
				//print_r($matches);die();
				$ANO = $matches[0][1];
				$MES = $matches[0][2];
				$IDNAME = $matches[0][3];
				include(ROOT_PAGES . "/site/cobertura.php");
				exit;
			}

			# EVENTOS ==================================================================================================
			if(preg_match_all("/^(Eventos|Agenda)$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/site/eventos.php");
				exit;
			}
			/*if(preg_match_all("/(Eventos|Agenda)\/(.*)/i", $URL, $matches,PREG_SET_ORDER)) // Macete: Não usar o "^" no inicio, para nao configurar que é no inicio da string
			{
				//print_r($matches);die();
				$IDNAME = $matches[0][2];
				include(ROOT_PAGES . "/site/evento.php");
				exit;
			}	*/
			//Novo formato para poder funcionar as avaliacoes ?usuario_id=XXX (inserido em 10/11/2015)
			if(preg_match_all("/(Eventos|Agenda)\/(.*)/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				$STR = parse_url($URL, PHP_URL_QUERY);
				parse_str($STR,$OUTPUT); //Converte str como se ela tivesse sido passada via URL e define o valor das variáveis.  Ex: ?offset=20&categoria=Saia
				$parts = explode("?",$matches[0][2]);
				$IDNAME = $parts[0];

				include(ROOT_PAGES . "/site/evento.php");
				exit;
			}



			# LOJA ======   ÓTIMA SOLUCAO CRIADA (PEGAR DADOS TIPO GET através de parse_url) ::  Vitor, não deixe de utilizar nos outros sistemas, leia o paragrafo abaixo ===
			# Solucao muito boa que pode ser utilizada em todos os outros sistemas | EClubes | ShowCommerce | Bizu
			# Passo os dados via GET e uso o "parse_url" e "parse_str" para pegar esse valores
			# em 09/09/2014
			# =================================================================================================================================================================
			if(preg_match_all("/^Loja/i", $URL, $matches,PREG_PATTERN_ORDER))  //  /^STRING$/  Exatamente
			{
				$STR = parse_url($URL, PHP_URL_QUERY); //Retorna tudo o que vier depois do Interrogacao: "site/loja?"
				parse_str($STR,$OUTPUT); //Converte str como se ela tivesse sido passada via URL e define o valor das variáveis.  Ex: ?offset=20&categoria=Saia
				include(ROOT_PAGES . "/site/loja.php");
				exit;
			}
			if(preg_match_all("/^Produto\/(.*)$/i", $URL, $matches,PREG_SET_ORDER))
			{
				//print_r($matches);
				$IDNAME = $matches[0][1];
				include(ROOT_PAGES . "/site/loja_produto.php");
				exit;

			}


			#------------------------
			# PERFIL DO CLIENTE
			#------------------------
			if(preg_match_all("/^cliente$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				//print_r($matches);
				include(ROOT_PAGES . "/cliente/pedidos.php");
				exit;
			}
			if(preg_match_all("/^cliente\/dados$/i", $URL, $matches,PREG_SET_ORDER)) // Macete: Não usar o "^" no inicio, para nao configurar que é no inicio da string
			{
				include(ROOT_PAGES . "/cliente/dados.php");
				exit;
			}
			if(preg_match_all("/^cliente\/pedidos$/i", $URL, $matches,PREG_SET_ORDER)) // Macete: Não usar o "^" no inicio, para nao configurar que é no inicio da string
			{
				//print_r($matches);
				include(ROOT_PAGES . "/cliente/pedidos.php");
				exit;
			}
			if(preg_match_all("/^cliente\/pedidos\/(.*)?/i", $URL, $matches,PREG_SET_ORDER)) // Macete: Não usar o "^" no inicio, para nao configurar que é no inicio da string
			{
				//print_r($matches);
				$PEDIDO_ID = $matches[0][1];
				include(ROOT_PAGES . "/cliente/pedido.php");
				exit;
			}
			# VIDEOS ==================================================================================================
			if(preg_match_all("/^Videos$/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				include(ROOT_PAGES . "/site/videos.php");
				exit;
			}


			#------------------------
			# COMISSARIOS
			#------------------------
			if(preg_match_all("/^(.*)/i", $URL, $matches,PREG_SET_ORDER))  //  /^STRING$/  Exatamente
			{
				//print_r($matches);
				$NOME_USUARIO = $matches[0][1];
				include(ROOT_PAGES . "/site/comissario.php");
				exit;
			}

			include(ROOT_PAGES . "/erro/404.php");
			exit;

}else{

	include(ROOT_PAGES . "/index.php");
	exit;
}

?>
