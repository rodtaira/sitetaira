<?php
/*
 * funcoes Gerais da APP
 */
/*
 * SESSION // NAVIGATION HASH ;; include_session()

 * <PORQUE>
 * Caso a pagina seja uma SESSAO(Abas) //
 * Devido ao tipo de carregamento jquery.load() tenho que criar uma session para garantir que as abas tratem do mesmo objeto

 * <EXEMPLO>
 * Na pagina de @gerenciador/USUARIO.php?id=123
 * executo o include_session_navigation('usuario','get','123')
 * tenho as abas: 1)Dados pessoais / 2) Dados de acesso/  3) Pedidos realizados / 4) Pagamentos realizados
 * Atraves da SESSION todas essas paginas externas vao entender que se tratam do objeto usuario id=123, pois o 123 foi passado para a session

 * Cria uma session com os parametros passados
 */
/*
 * HEADER :: NAO VOU MAIS UTILIZAR
 * Inclui o Header de acordo com o tema escolhido na funcao tema()
 * Caso o tema seja invalido, Redireciona para erro_tema.php
 */
/*function include_header()
{

	global $INI; // header na raiz // 1 mesmo header para o site todo
	if(is_dir(ROOT_SITE)){
	 include(ROOT_SITE . "/header.php");
	}else{
		echo getTemplate(ROOT_SITE."/pages/erro/erro_tema.php");
		die();
		}
}
* FIM HEADER
*/

function __autoload($classe)
{
	if (file_exists( ROOT_APP . "/php/{$classe}.class.php"))
	{
		include_once ROOT_APP . "/php/{$classe}.class.php";
	}
}

//PARAMETROS - Carrega os parametros e alimenta a session INI e o próprio array INI
function getParams()
{
	global $INI;
	$requisicao = new ShowCommerceRequest('db.GetParams');
	//$stdClassDB = $requisicao->dump = TRUE;
	$stdClassDB = $requisicao->start();


	//CASO TENHA ERRO DE PERMISSAO, PÁRA A PAGINA (Erros de credenciais)
	if(isset($stdClassDB['erro']) && $stdClassDB['erro']==true)
	{
		switch($stdClassDB['cod_erro'])
		{
			case "R004":
			echo getTemplate(ROOT_SITE."/pages/erro/suspenso.php");
			echo $stdClassDB['mensagem'];
			exit();
			break;
		}
		echo $stdClassDB['mensagem'];
		exit;
	};
	@session_start();
	//ANALYTICS
	$_SESSION['INI']['analytics']['codigo'] 					= $INI['analytics']['codigo'] 					= $stdClassDB['analytics']->codigo;
	$_SESSION['INI']['google']['conversion_id'] 				= $INI['google']['conversion_id'] 				= $stdClassDB['google']->conversion_id;
	$_SESSION['INI']['google']['conversion_label'] 				= $INI['google']['conversion_label'] 			= $stdClassDB['google']->conversion_label;
	$_SESSION['INI']['facebook']['conversion_code'] 			= $INI['facebook']['conversion_code'] 			= $stdClassDB['facebook']->conversion_code;
	$_SESSION['INI']['facebook']['pixel_code'] 					= $INI['facebook']['pixel_code'] 				= $stdClassDB['facebook']->pixel_code;
	//INI EMAIL
	//$_SESSION['INI']['email']['nome'] 						= $INI['email']['nome']							= $stdClassDB['email']->nome;
	//$_SESSION['INI']['email']['from'] 						= $INI['email']['from']							= $stdClassDB['email']->from;
	//$_SESSION['INI']['email']['reply'] 						= $INI['email']['reply'] 						= $stdClassDB['email']->reply;
	//$_SESSION['INI']['email']['user'] 						= $INI['email']['user'] 						= $stdClassDB['email']->user;
	//$_SESSION['INI']['email']['pass']							= $INI['email']['pass'] 						= $stdClassDB['email']->pass;
	//$_SESSION['INI']['email']['tipo'] 						= $INI['email']['tipo'] 						= $stdClassDB['email']->tipo;
	//$_SESSION['INI']['email']['host'] 						= $INI['email']['host']							= $stdClassDB['email']->host;
	//$_SESSION['INI']['email']['timezone'] 					= $INI['email']['timezone'] 					= $stdClassDB['email']->timezone;
	//$_SESSION['INI']['email']['port'] 						= $INI['email']['port']							= $stdClassDB['email']->port;
	//$_SESSION['INI']['email']['charset'] 						= $INI['email']['charset']						= $stdClassDB['email']->charset;
	$_SESSION['INI']['email']['suporte'] 						= $INI['email']['suporte'] 						= $stdClassDB['email']->suporte;
	$_SESSION['INI']['email']['sac'] 							= $INI['email']['sac'] 							= $stdClassDB['email']->sac;
	//$_SESSION['INI']['email']['reservas'] 					= $INI['email']['reservas'] 					= $stdClassDB['email']->reservas;
	//CLIENTE
	$_SESSION['INI']['cliente']['nome'] 						= $INI['cliente']['nome'] 						= $stdClassDB['cliente']->nome;
	$_SESSION['INI']['cliente']['endereco'] 					= $INI['cliente']['endereco'] 					= $stdClassDB['cliente']->endereco;
	$_SESSION['INI']['cliente']['cep'] 							= $INI['cliente']['cep'] 						= $stdClassDB['cliente']->cep;
	$_SESSION['INI']['cliente']['dominio'] 						= $INI['cliente']['dominio'] 					= $stdClassDB['cliente']->dominio;
	$_SESSION['INI']['cliente']['email'] 						= $INI['cliente']['email'] 						= $stdClassDB['cliente']->email;
	$_SESSION['INI']['cliente']['telefones'] 					= $INI['cliente']['telefones'] 					= $stdClassDB['cliente']->telefones;
	$_SESSION['INI']['cliente']['logo'] 						= $INI['cliente']['logo'] 						= $stdClassDB['cliente']->logo;
	$_SESSION['INI']['cliente']['historico'] 					= $INI['cliente']['historico'] 					= $stdClassDB['cliente']->historico;
	$_SESSION['INI']['cliente']['portfolio'] 					= $INI['cliente']['portfolio'] 					= $stdClassDB['cliente']->portfolio;
	$_SESSION['INI']['cliente']['servico'] 						= $INI['cliente']['servico'] 					= $stdClassDB['cliente']->servico;
	$_SESSION['INI']['cliente']['encomendas'] 					= $INI['cliente']['encomendas'] 				= $stdClassDB['cliente']->encomendas;
	$_SESSION['INI']['cliente']['suporte_telefones'] 			= $INI['cliente']['suporte_telefones'] 			= $stdClassDB['cliente']->suporte_telefones;
	$_SESSION['INI']['cliente']['suporte_horario'] 				= $INI['cliente']['suporte_horario'] 			= $stdClassDB['cliente']->suporte_horario;
	$_SESSION['INI']['cliente']['politica_compras'] 			= $INI['cliente']['politica_compras'] 			= $stdClassDB['cliente']->politica_compras;

	//SITE
	$_SESSION['INI']['site']['status'] 							= $INI['site']['status'] 						= $stdClassDB['site']->status;
	$_SESSION['INI']['site']['tema'] 							= $INI['site']['tema'] 							= $stdClassDB['site']->tema;
	$_SESSION['INI']['site']['nome'] 							= $INI['site']['nome'] 							= $stdClassDB['site']->nome;
	$_SESSION['INI']['site']['titulo'] 							= $INI['site']['titulo'] 						= $stdClassDB['site']->titulo;
	$_SESSION['INI']['site']['charset'] 						= $INI['site']['charset'] 						= $stdClassDB['site']->charset;
	$_SESSION['INI']['site']['keywords'] 						= $INI['site']['keywords'] 						= $stdClassDB['site']->keywords;
	$_SESSION['INI']['site']['descricao'] 						= $INI['site']['descricao'] 					= $stdClassDB['site']->descricao;
	//SOCIAL
	$_SESSION['INI']['social']['twitter'] 						= $INI['social']['twitter'] 					= $stdClassDB['social']->twitter;
	$_SESSION['INI']['social']['facebook'] 						= $INI['social']['facebook'] 					= $stdClassDB['social']->facebook;
	$_SESSION['INI']['social']['facebook_album_id'] 			= $INI['social']['facebook_album_id'] 			= $stdClassDB['social']->facebook_album_id;
	$_SESSION['INI']['social']['instagram'] 					= $INI['social']['instagram'] 					= $stdClassDB['social']->instagram;
	$_SESSION['INI']['social']['youtube'] 						= $INI['social']['youtube'] 					= $stdClassDB['social']->youtube;
	//DEFAULTS
	$_SESSION['INI']['defaults']['cidade_id'] 					= $INI['defaults']['cidade_id'] 				= $stdClassDB['defaults']->cidade_id;
	$_SESSION['INI']['defaults']['limite_ingressos'] 			= $INI['defaults']['limite_ingressos'] 			= $stdClassDB['defaults']->limite_ingressos;
	$_SESSION['INI']['defaults']['tempo_reserva'] 				= $INI['defaults']['tempo_reserva'] 			= $stdClassDB['defaults']->tempo_reserva;
	$_SESSION['INI']['defaults']['conveniencia'] 				= $INI['defaults']['conveniencia'] 				= $stdClassDB['defaults']->conveniencia;
	$_SESSION['INI']['defaults']['frete_gratis'] 				= $INI['defaults']['frete_gratis'] 				= $stdClassDB['defaults']->frete_gratis;
	$_SESSION['INI']['defaults']['frete_servicos'] 				= $INI['defaults']['frete_servicos'] 			= $stdClassDB['defaults']->frete_servicos;

	//PAGAMENTOS
	//passepague
	$_SESSION['INI']['pagamentos']['passepague_publicar'] 		= $INI['pagamentos']['passepague_publicar'] 	= $stdClassDB['pagamentos']->passepague_publicar;
	$_SESSION['INI']['pagamentos']['passepague_max_parcelas'] 	= $INI['pagamentos']['passepague_max_parcelas'] = $stdClassDB['pagamentos']->passepague_max_parcelas;
	$_SESSION['INI']['pagamentos']['passepague_juros'] 			= $INI['pagamentos']['passepague_juros'] 		= $stdClassDB['pagamentos']->passepague_juros;
	//rede
	$_SESSION['INI']['pagamentos']['rede_publicar'] 			= $INI['pagamentos']['rede_publicar'] 			= $stdClassDB['pagamentos']->rede_publicar;
	$_SESSION['INI']['pagamentos']['rede_max_parcelas'] 		= $INI['pagamentos']['rede_max_parcelas'] 		= $stdClassDB['pagamentos']->rede_max_parcelas;
	$_SESSION['INI']['pagamentos']['rede_parcelamento'] 		= $INI['pagamentos']['rede_parcelamento'] 		= $stdClassDB['pagamentos']->rede_parcelamento;
	//pagseguro
	$_SESSION['INI']['pagamentos']['pagseguro_publicar'] 		= $INI['pagamentos']['pagseguro_publicar'] 		= $stdClassDB['pagamentos']->pagseguro_publicar;
	//bilheteria



	//Constantes
	$_SESSION['INI']['DIR_UPLOADS'] = $stdClassDB['DIR_UPLOADS'];
	define( 'DIR_UPLOADS',	$stdClassDB['DIR_UPLOADS']); //utilizo para buscar a orientacao da foto. Usando o THUMB não consigo descobrir

	$_SESSION['INI']['FILE_THUMBS'] = $stdClassDB['FILE_THUMBS'];
	define( 'THUMB',	$stdClassDB['FILE_THUMBS'] 	); //http://showcommerce-files.net.br/thumbs

	$_SESSION['INI']['DIR_STATIC']  = $stdClassDB['DIR_STATIC'];
	define( 'CONTENTS',	$stdClassDB['DIR_STATIC'] 	); //http://showcommerce.com.br/pages/static (Para ser usado com file_get_contents())


}

//SET ARRAY $INI - Atribui os valores da Session['INI'] para o array INI
function setINI($SESSION_INI)
{
	global $INI;

	//ANALYTICS
	$INI['analytics']['codigo']					= $SESSION_INI['analytics']['codigo'];
	$INI['google']['conversion_id']				= $SESSION_INI['google']['conversion_id'];
	$INI['google']['conversion_label']			= $SESSION_INI['google']['conversion_label'];
	$INI['facebook']['conversion_code']			= $SESSION_INI['facebook']['conversion_code'];
	$INI['facebook']['pixel_code']				= $SESSION_INI['facebook']['pixel_code'];
	//INI EMAIL
	//$INI['email']['nome']						= $SESSION_INI['email']['nome'];
	//$INI['email']['from']						= $SESSION_INI['email']['from'];
	//$INI['email']['reply']					= $SESSION_INI['email']['reply'];
	//$INI['email']['user'] 					= $SESSION_INI['email']['user'];
	//$INI['email']['pass'] 					= $SESSION_INI['email']['pass'];
	//$INI['email']['tipo'] 					= $SESSION_INI['email']['tipo'];
	//$INI['email']['host']				 		= $SESSION_INI['email']['host'];
	//$INI['email']['timezone'] 				= $SESSION_INI['email']['timezone'];
	//$INI['email']['port']					 	= $SESSION_INI['email']['port'] ;
	//$INI['email']['charset'] 					= $SESSION_INI['email']['charset'];
	$INI['email']['suporte']				 	= $SESSION_INI['email']['suporte'];
	$INI['email']['sac']				 		= $SESSION_INI['email']['sac'] ;
	//$INI['email']['reservas'] 				= $SESSION_INI['email']['reservas'];
	//CLIENTE
	$INI['cliente']['nome']						= $SESSION_INI['cliente']['nome'];
	$INI['cliente']['endereco']					= $SESSION_INI['cliente']['endereco'];
	$INI['cliente']['cep']						= $SESSION_INI['cliente']['cep'];
	$INI['cliente']['dominio']					= $SESSION_INI['cliente']['dominio'];
	$INI['cliente']['email']					= $SESSION_INI['cliente']['email'];
	$INI['cliente']['telefones']				= $SESSION_INI['cliente']['telefones'];
	$INI['cliente']['logo']						= $SESSION_INI['cliente']['logo'];
	$INI['cliente']['historico']				= $SESSION_INI['cliente']['historico'];
	$INI['cliente']['portfolio']				= $SESSION_INI['cliente']['portfolio'];
	$INI['cliente']['servico']					= $SESSION_INI['cliente']['servico'];
	$INI['cliente']['encomendas']				= $SESSION_INI['cliente']['encomendas'];
	$INI['cliente']['suporte_telefones']		= $SESSION_INI['cliente']['suporte_telefones'];
	$INI['cliente']['suporte_horario']			= $SESSION_INI['cliente']['suporte_horario'];
	$INI['cliente']['politica_compras']			= $SESSION_INI['cliente']['politica_compras'];

	//SITE
	$INI['site']['tema'] 						= $SESSION_INI['site']['tema'];
	$INI['site']['nome'] 						= $SESSION_INI['site']['nome'];
	$INI['site']['titulo']  					= $SESSION_INI['site']['titulo'];
	$INI['site']['charset'] 					= $SESSION_INI['site']['charset'];
	$INI['site']['keywords'] 					= $SESSION_INI['site']['keywords'];
	$INI['site']['descricao']  					= $SESSION_INI['site']['descricao'];
	$INI['site']['status'] 						= $SESSION_INI['site']['status'];
	//SOCIAL
	$INI['social']['twitter'] 					= $SESSION_INI['social']['twitter'];
	$INI['social']['facebook']  				= $SESSION_INI['social']['facebook'];
	$INI['social']['facebook_album_id'] 		= $SESSION_INI['social']['facebook_album_id'];
	$INI['social']['instagram']  				= $SESSION_INI['social']['instagram'];
	$INI['social']['youtube']  					= $SESSION_INI['social']['youtube'];
	//DEFAULTS
	$INI['defaults']['cidade_id'] 				= $SESSION_INI['defaults']['cidade_id'];
	$INI['defaults']['limite_ingressos'] 		= $SESSION_INI['defaults']['limite_ingressos'];
	$INI['defaults']['tempo_reserva'] 			= $SESSION_INI['defaults']['tempo_reserva'];
	$INI['defaults']['conveniencia'] 			= $SESSION_INI['defaults']['conveniencia'];
	$INI['defaults']['frete_gratis'] 			= $SESSION_INI['defaults']['frete_gratis'];
	$INI['defaults']['frete_servicos'] 			= $SESSION_INI['defaults']['frete_servicos'];
	//PAGAMENTOS
	//passepague
	$INI['pagamentos']['passepague_publicar']		= $_SESSION['INI']['pagamentos']['passepague_publicar'];
	$INI['pagamentos']['passepague_max_parcelas']	= $_SESSION['INI']['pagamentos']['passepague_max_parcelas'];
	$INI['pagamentos']['passepague_juros']			= $_SESSION['INI']['pagamentos']['passepague_juros'];
	//pagseguro
	$INI['pagamentos']['pagseguro_publicar'] 		= $_SESSION['INI']['pagamentos']['pagseguro_publicar'];
	//rede
	$INI['pagamentos']['rede_publicar'] 			= $_SESSION['INI']['pagamentos']['rede_publicar'];
	$INI['pagamentos']['rede_max_parcelas'] 		= $_SESSION['INI']['pagamentos']['rede_max_parcelas'];
	$INI['pagamentos']['rede_parcelamento'] 		= $_SESSION['INI']['pagamentos']['rede_parcelamento'];
	//bilheteria

	//Constantes
	define( 'THUMB',		$SESSION_INI['FILE_THUMBS']);
	define( 'DIR_UPLOADS',	$SESSION_INI['DIR_UPLOADS']); //utilizo para buscar a orientacao da foto. Usando o THUMB não consigo descobrir
	define( 'CONTENTS',		$SESSION_INI['DIR_STATIC'] 	); //http://showcommerce.com.br/pages/static (Para ser usado com file_get_contents())

}

//PIXEL FACEBOOK // APARECE DE ACORDO COM A PAGINA ATUAL
function pixelFacebook($pixel_code='',$pagina_atual='',$parametros=array())
{
	//breaks
	if(!$pixel_code || !$pagina_atual)
	{
		return false;
	}

	//INICIO DO SCRIPT
	$SCRIPT  = "<!--FACEBOOK PIXEL-->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '{$pixel_code}');";

	switch($pagina_atual)
	{
		case "evento":
			$SCRIPT .= "fbq('track', 'ViewContent');";
		break;
		case "carrinho":
			$SCRIPT .= "fbq('track', 'AddToCart');";
		break;
		case "checkout":
			$SCRIPT .= "fbq('track', 'AddPaymentInfo');";
		break;
		case "retorno_pagamento":
			$valor = (isset($parametros['valor'])) ? $parametros['valor'] : "0.00";
			$moeda = (isset($parametros['moeda'])) ? $parametros['moeda'] : "BRL";
			$SCRIPT .= "fbq('track', 'Purchase', {value: '{$valor}', moeda: '{$moeda}'});";
		break;
	}

	//FIM DO SCRIPT
	$SCRIPT .= "</script>
	<noscript><img height='1' width='1' style='display:none'
	src='https://www.facebook.com/tr?id={$pixel_code}&ev=PageView&noscript=1'
	/></noscript>";

	echo $SCRIPT;
}

//LOGA O USUARIO NO SISTEMA (Cria Session)
function loginUsuario($Usuario)
{
	@session_start();

	include_session($Usuario->id,"login","id");
	include_session($Usuario->nome,"login","nome");
	include_session($Usuario->sobrenome,"login","sobrenome");
	include_session($Usuario->email,"login","email");
	include_session($Usuario->url_thumb,"login","avatar");

	include_session('usuario',"login","tipo");

}

function printAreaLogin() //Imprime os botoes de login e cadastro
{
	/*Essa área de Login (ToolTip) deve estar dentro do BODY, mas fora do Header
	* Pois deve ter o espaço absoluto livre para exibir o tooltip. Sempre em relacao ao Body*/

	@session_start();
	//AREA LOGIN
	if(isset($_SESSION['login']['id']))
	{
		   //LOGADO
			//ToolTip Escondido (modal)
			$tooltip_login = "<div class='tooltipLogin' id='tooltip_perfil_usuario'>";
			$tooltip_login .= "<div class='pad20'>";
			$tooltip_login .= "<div class='thumb coluna left mr20 w30 h30 rad100' style='background-image:url({$_SESSION['login']['avatar']})'></div>";
			$tooltip_login .= "<div class='coluna'>";
			$tooltip_login .= "<strong class='f11'>{$_SESSION['login']['nome']} {$_SESSION['login']['sobrenome']}</strong><br /><span class='f10'>{$_SESSION['login']['email']}</span>";
			$tooltip_login .= "</div>";
			$tooltip_login .= "<div class='clear'></div>";
			//menu nav
			$tooltip_login .= "<div class='tooltip-menu'>";
			$tooltip_login .= "<a href='".PAGE_CLIENTE."/dados'><i class='icon-user'></i> Meus dados</a>";//
			$tooltip_login .= "<a href='".PAGE_CLIENTE."/pedidos'><i class='icon-shopping-cart'></i> Meus pedidos</a>";//
			$tooltip_login .= "</div>";
			$tooltip_login .= "</div>";
			$tooltip_login .= "<div class='aright st1' style='border-top:1px solid #bbb; padding:10px 20px; background:#eee'>";
			$tooltip_login .= "<a class='btn btn-danger btn-mini' href='".URL_SITE."/logout'><i class='icon-white icon-off'></i> sair</a>";
			$tooltip_login .= "</div>";
			$tooltip_login .= "</div>";
			echo $tooltip_login;

			//Exibe Avatar
			echo "<div class='login'>";
			echo "<a class='wtoggle avatar' wtoggle-target='tooltip_perfil_usuario' style='background-image:url({$_SESSION['login']['avatar']});'></a>";
			echo "</div>";
	}else{
			//SEM SESSAO
			//Janela
			$tooltip_login = "<div class='tooltipLogin' id='tooltip_login'>";
			$tooltip_login .= "<form action='login' class='ajax mt5 w350 pad10' name='login_header'>";
			$tooltip_login .= "<fieldset>";
			$tooltip_login .= "<input type='text' name='chave' placeholder='cpf ou email' class='required left w120 rad3' style='float:left !important;'>";
			$tooltip_login .= "<input type='password' name='senha' placeholder='senha' class='required left w120 rad3'>";
			$tooltip_login .= "<input type='submit' value='entrar' class='btn btn-warning left'>";
			$tooltip_login .= "<div class='clear'></div><a href='".URL_SITE."/EsqueciMinhaSenha' class='famarela f10'>Esqueci minha senha</a>";
			$tooltip_login .= "</fieldset>";

			$tooltip_login .= "<br /><a class='btn btn-primary' href='".LOGIN_FACE."?requisicao_cliente=".CLIENTE_ID."&url_retorno=".PAGE_RETORNO_FACEBOOK."'><i class='icon-face icon-white'></i> Logar com Facebook</a>";
			//$tooltip_login .= "<br /><a href='".LOGIN_FACE."?requisicao_cliente=".CLIENTE_ID."&url_retorno=".PAGE_RETORNO_FACEBOOK."'><img src='".DIR_THEME."/img/icones/bt_fb_login.png' border='0'></a>";
			$tooltip_login .= "</form>";
			$tooltip_login .= "<div class='clear'></div>";
			$tooltip_login .= "<div class='aright st1' style='border-top:1px solid #bbb; padding:10px 20px; background:#eee'>";
			$tooltip_login .= "Ainda não é cadastrado ? <a href='".URL_SITE."/cadastro' class='famarela'>Cadastre-se</a><br />";
			$tooltip_login .= "</div>";
			$tooltip_login .= "</div>";
			echo $tooltip_login;

			//Exibe Botão Login
			echo "<div class='login'>";
			echo "<div class='botoes'>";
			echo "<a href='#' class='wtoggle' wtoggle-target='tooltip_login'>LOGIN</a>";
			//echo "<a href='".PAGE_CADASTRO."' class='btn-cadastro'>CADASTRE-SE</a>";
			echo "</div>";
			echo "</div>";
	}

}

function getBrowser()
{
	$useragent = $_SERVER['HTTP_USER_AGENT'];

	if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched))
	{
		$browser_version=$matched[1];
		$browser = 'IE';
	}
	elseif (preg_match( '|Opera/([0-9].[0-9]{1,2})|',$useragent,$matched))
	{
		$browser_version=$matched[1];
		$browser = 'Opera';
	}
	elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched))
	{
		$browser_version=$matched[1];
		$browser = 'Firefox';
	}
	elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched))
	{
		$browser_version=$matched[1];
		$browser = 'Chrome';
	}
	elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched))
	{
		$browser_version=$matched[1];
		$browser = 'Safari';
	}
	else
	{
		$browser_version = 0;
		$browser= 'other';
	}

	return "$browser $browser_version";
}


function includeErro($codigo='404')
{
	switch($codigo)
	{
		case "404":
			include(ROOT_PAGES . "/erro/404.php");
			die();
		break;
	}
}

function getQtdMaximaProduto($maxProduto) //Informa a quantidade maxima de compras, cruzando o parametro "$Produto->max" com o $INI['defaults']['limite_ingressos']
{
	global $INI;
	$qtd_maxima	 = ($maxProduto>0 && $maxProduto < $INI['defaults']['limite_ingressos']) ?  $maxProduto : $INI['defaults']['limite_ingressos'];
	$qtd_maxima  = ($qtd_maxima) ? $qtd_maxima : 1;
	return $qtd_maxima;
}

/*
 * TEMPLATE
 * Carrega o arquivo html ou php e retorna o codigo fonte como um template
 */
function getTemplate($file)
{
    ob_start(); // inicia output buffer
    include($file);
    // armazena a movimentacao do BUFFER php até agora. // No caso so o resultado do include($file)
	$template = ob_get_contents();
    ob_end_clean();
    return $template;
}


//Adiciona template para enviar por email ou exibir na pagina
function addTemplate($template, $vars)
{
	foreach($vars AS $chave=>$value){
		$$chave = $value;
	}
	$template = file_get_contents($template);
	$template = preg_replace("/\{([^\{]{1,100}?)\}/e","$$1",$template);
	return $template;
}


/*
 * INCLUDE SESSION
 * o mesmo do session navigation. So que global
 */
function include_session( $valor="", $nivel1="navigation", $nivel2="")
{
	@session_start();

	if($nivel1=="navigation")
	{
		if(!defined('SESSION_NAVIGATION')){
			define("SESSION_NAVIGATION",true); // A constante será testada nos headers da aplicação. Caso exista, insere na tag <body> onhashchange="HashChangeHandler();" onLoad="HashChangeHandler();
		}
	}

	if($valor!="") // so trabalha se vier valor, senao so vai startar a sessao mesmo
	{
		if($nivel2!="") $_SESSION[$nivel1][$nivel2] = $valor;
		else     		$_SESSION[$nivel1] = $valor;
	}
}

/*
 * LIMPA SESSION
 */
function limpa_session($session="")
{
	@session_start();
	if($session==""){
		unset($_SESSION);
	}else{
		unset($_SESSION[$session]);
		}
}

/*
 * RENDER JQUERY
 * Com base no tema escolhido, executa o arquivo com as configurações do JQUERY
 * renderiza novamente utilizando o $(document).ready()
 */
function render_jquery()
{
	echo "<script src='".DIR_THEME."/widget/widgets.js?".DIR_THEME."'></script>";
	/*echo "<script src='".DIR_LIB."/jquery/configuracoes.js?".URL_SITE."'></script>";*/
}

function filtro_string_tira_acento($string)
{
	// Tira todos os acentos
	$a = array("á","ã","â","à","ä","Ä","À","Á","Ã","Â");
	$e = array("é","ê","è","ë","Ë","Ê","È","É");
	$i = array("í","ì","ï","Ï","Î","Í","Ì");
	$o = array("ó","õ","ô","ò","ö","Ö","Ò","Ó","Õ","Ô");
	$u = array("ú","ü","ù","Ú","Ù","û");
	$c = array("ç","Ç");

	$tratar = str_replace($a,"a", $string);
	$tratar = str_replace($e,"e", $tratar);
	$tratar = str_replace($i,"i", $tratar);
	$tratar = str_replace($o,"o", $tratar);
	$tratar = str_replace($u,"u", $tratar);
	$string_sem_acentos = str_replace($c,"c", $tratar);
	return  $string_sem_acentos;
}

function filtro_string_regexp($query)
{
	$string = trim($query);
	$string_sem_acentos = filtro_string_tira_acento($string);
	// Cria sequencias para cada volgal ou "c"
	$a = "a|á|ã|â|à|ä|Ä|À|Á|Ã|Â";
	$e = "e|é|ê|è|ë|Ë|Ê|È|É";
	$i = "i|í|ì|ï|Ï|Î|Í|Ì";
	$o = "o|ó|õ|ô|ò|ö|Ö|Ò|Ó|Õ|Ô";
	$u = "u|ú|ü|ù|Ú|Ù|û";
	$c = "c|ç|Ç";
	$tratar = str_replace("a","[$a]*", $string_sem_acentos);
	$tratar = str_replace("e","[$e]*", $tratar);
	$tratar = str_replace("i","[$i]*", $tratar);
	$tratar = str_replace("o","[$o]*", $tratar);
	$tratar = str_replace("u","[$u]*", $tratar);
	$tratar = str_replace("c","[$c]*", $tratar);
	$query_tratada = $tratar;

	return  $query_tratada;
}

function filtro_titulo($titulo)
{
	$novo_titulo = filtro_string_tira_acento($titulo);
	$novo_titulo = str_replace(" ","-",$novo_titulo);
	$novo_titulo = str_replace("---","-",$novo_titulo);
	$novo_titulo = str_replace("--","-",$novo_titulo);
	return  $novo_titulo;
}

function restrito($tipos="")  //"cliente,administrador,superadmin";
{
	@session_start();
	$_SESSION['paginaRetorno'] = URL_SITE."/".$_SERVER['REQUEST_URI'];

	if(!isset($_SESSION['login']))
	{
		//header("Location: ".PAGE_LOGIN);
		//include(ROOT_PAGES . "/erro/restrito.php");die();
		echo "<script>window.location.href='".PAGE_LOGIN."';</script>";
	}
}

function paginaRetorno() //Faz dessa uma pagina retorno
{
	@session_start();
	$_SESSION['paginaRetorno'] = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
}


//Para Buscas (Destaque)
function destaque_resultado($frase,$string_destaque)
{
	$resultado = str_ireplace("$string_destaque","<span class='fdestaque'>$string_destaque</span>",$frase);
	return $resultado;
}

function tratamento_caracteres($string,$space=true) //Retorna string sem caracteres especiais, acentos e espacos
{
	$a = array("á","ã","â","à","ä","Ä","À","Á","Ã","Â");
	$e = array("é","ê","è","ë","Ë","Ê","È","É");
	$i = array("í","ì","ï","Ï","Î","Í","Ì");
	$o = array("ó","õ","ô","ò","ö","Ö","Ò","Ó","Õ","Ô");
	$u = array("ú","ü","ù","Ú","Ù","û");
	$c = array("ç","Ç");
	$especiais = array(",","`","~","^","'",";",":","]","[","!","?","&","–","º","ª","/","\\");
	$espaco = array(" ");

	$tratamento = str_replace($a,"a", $string);
	$tratamento = str_replace($e,"e", $tratamento);
	$tratamento = str_replace($i,"i", $tratamento);
	$tratamento = str_replace($o,"o", $tratamento);
	$tratamento = str_replace($u,"u", $tratamento);
	$tratamento = str_replace($c,"c", $tratamento);
	$tratamento = str_replace($especiais,"", $tratamento);

	//ESPACOS ENTRE PALAVRAS
	if($space==true){
		$tratamento = str_replace($espaco,"_", $tratamento);
	}else{
		$tratamento = str_replace($espaco,"", $tratamento);
	}

	$nometratado = strtolower($tratamento);
	return $nometratado;
}

function only_numbers($string) //Retorna string sem caracteres especiais, acentos e espacos
{
	$resultado = preg_replace("/[^0-9]/", "",$string);
	return $resultado;
}

function mask($val, $mask)
{
	/*EXEMPLOS UTILIZACAO
	* $cnpj = "11222333000199";
	* $cpf = "00100200300";
	* $cep = "08665110";
	* $data = "10102010";
	* echo mask($cnpj,'##.###.###/####-##');
	* echo mask($cpf,'###.###.###-##');
	* echo mask($cep,'#####-###');
	* echo mask($data,'##/##/####');

	* $data = "10102010";
	* echo mask($data,'##/##/####');
	* echo mask($data,'[##][##][####]');
	* echo mask($data,'(##)(##)(####)');
	*
	* $hora = "021050";
	* echo mask($hora,'Agora são ## horas ## minutos e ## segundos');
	* echo mask($hora,'##:##:##');

	*/

	$maskared = '';
	$k = 0;
	for($i = 0; $i<=strlen($mask)-1; $i++)
	{
		if($mask[$i] == '#')
		{
			if(isset($val[$k]))
			$maskared .= $val[$k++];
		}else{
			if(isset($mask[$i]))
			$maskared .= $mask[$i];
		}
	}
	return $maskared;
}

function gera_voucher($id="")
{
	if($id=="")$id = time() . rand();// Basicamente a mesma teoria do gerarId() do TRecord
	$voucher = strtoupper(substr(md5( $id . time() ),0,7));
	return $voucher;
}

function valida_cpf($cpf)
{
		$cpf = only_numbers($cpf);
		// Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
		if (strlen($cpf) != 11
			|| 	$cpf == '00000000000'
			|| $cpf == '11111111111'
			|| $cpf == '22222222222'
			|| $cpf == '33333333333'
			|| $cpf == '44444444444'
			|| $cpf == '55555555555'
			|| $cpf == '66666666666'
			|| $cpf == '77777777777'
			|| $cpf == '88888888888'
			|| $cpf == '99999999999')
		{
				return false;
		}else{
				// Calcula os números para verificar se o CPF é verdadeiro
				for ($t = 9; $t < 11; $t++)
				{
						for ($d = 0, $c = 0; $c < $t; $c++) {
							$d += $cpf{$c} * (($t + 1) - $c);
						}

						$d = ((10 * $d) % 11) % 10;

						if ($cpf{$c} != $d) {
							return false;
						}
				}
				return true;
		}
}


function setPartsTel($numero) 	//fragmenta em DDD e telefone
{

	$numero = only_numbers($numero);
	$numero = ltrim($numero, '0');//retira "0" do inicio


	switch(strlen($numero))
	{
		case 8://8digitos de tel
			$mascara = mask($numero, "########");
			$ddd 		= "";
			$telefone 	= $mascara;
		break;
		case 9://9digitos de tel (sao paulo)
			$mascara 	= mask($numero, "#########");
			$ddd 		= "";
			$telefone 	= $mascara;
		break;
		case 10://8digitos de tel e 2ddd
			$mascara 	= mask($numero, "##-########");
			$parts 		= explode("-", $mascara);
			$ddd 		= $parts[0];
			$telefone 	= $parts[1];
		break;
		case 11://9digitos de tel e 2ddd (sao paulo)
			$mascara 	= mask($numero, "##-#########");
			$parts 		= explode("-", $mascara);
			$ddd 		= $parts[0];
			$telefone 	= $parts[1];
		break;
	}


	$partsTel['ddd'] = $ddd;
	$partsTel['telefone'] = $telefone;
	return $partsTel;
}

function setTelefone($numero)
{
	$FINAL = "";
	$parts = setPartsTel($numero);
	if($parts['ddd'])	$FINAL .= "({$parts['ddd']}) ";
	if($parts['telefone'])$FINAL .= "{$parts['telefone']}";
	return $FINAL;
}


function calcula_desconto($valor, $valor_desconto)
{
	$x = ($valor_desconto*100)/$valor;
	return (100)-ceil($x);
}

function elapsed($date)
{
	$agora = date("Y-m-d H:i:s");
	$calc = time() - strtotime($date);
	if($calc<3600){
		 $sigla_intervalo = "i";
		 $tipo_intervalo = ($calc < 120 ) ? "minuto": "minutos";
	}elseif($calc<86400){
		 $sigla_intervalo = "h";
		 $tipo_intervalo = "horas";
	}else{
		 $sigla_intervalo = "d";
		 $tipo_intervalo = ($calc < 172800 ) ? "dia" : "dias";
	}
	$intervalo = intervalo_datas($date, $agora, $sigla_intervalo);
	return "$intervalo $tipo_intervalo";
}

function intervalo_datas($data_passado, $data_futuro, $intervalo)
{ // Calcula intervalo de datas // Retorna Ano, Mes, dia, hora, minuto ou segundo
/**
 * Calcula o intervalo entre duas datas no formato ISO, o intervalo é dado
 * no formato específicado em intevalor q pode ser
 * y - ano
 * m - meses
 * d - dias
 * h - horas
 * n - minutos
 * default ´se gundos
 *
 * @param string $data1
 * @param string $data2
 * @param string $intervalo m, d, h, n,y
 * @return int|string intervalo de horas

 * USO: $evento= '2010-04-24';
		echo intervalo_datas(date('Y-m-d'), $evento, 'd');

 */

    switch ($intervalo) {
        case 'y':
            $Q = 86400*365;
            break; //ano
        case 'm':
            $Q = 2592000;
            break; //mes
        case 'd':
            $Q = 86400;
            break; //dia
        case 'h':
            $Q = 3600;
            break; //hora
        case 'i':
            $Q = 60;
            break; //minuto
        default:
            $Q = 1;
            break; //segundo
    }

    return floor((strtotime($data_futuro) - strtotime($data_passado)) / $Q);
}

function to_double($string) // Transforma string em numero double ex: "R$50,500.36"  ---->  "50500.36"
{
	//Criado em 09/12/2013 Por Vitor
	//Verifica se tem sinal negativo
	$negativo = (strpos($string,"-")!==FALSE) ? "-" : "";


	//Filtra tudo que é string deixando apenas numeros, pontos e virgulas
	$string = preg_replace("/[^0-9\,\.]/", "",$string);

	//$num_pontos = substr_count($string, '.');//boa funcao - vai ficar de feedback
	//$num_virgulas = substr_count($string, ',');//boa funcao - vai ficar de feedback
	$ult_ponto = strripos($string,".");
	$ult_virgula = strripos($string,",");

	//IDENTIFICA O NUMERO INTEIRO ex: [2500 ]=> [add ".00"] => [2500.00]
	if($ult_ponto==false && $ult_virgula==false)
	{
			$string = number_format($string, 2, '.', '');
	}else{
			$tamanho = strlen($string);
			$ultimo = ($ult_ponto>$ult_virgula) ? "ponto" : "virgula";

			switch($ultimo)
			{
				case 'virgula':
				$string = substr_replace($string, '?', $ult_virgula,-($tamanho-$ult_virgula)+1);
				break;
				case 'ponto':
				$string = substr_replace($string, '?', $ult_ponto,-($tamanho-$ult_ponto)+1);
				break;
			}

			$string = str_replace('.','',$string);
			$string = str_replace(',','',$string);
			$string = str_replace('?','.',$string);
			$string = number_format($string, 2, '.', '');
	}

	return $negativo.$string;//aqui já é um double
}

function to_date($data) //Formato YYYY-mm-dd
{
	// Criado em 27/02/2014 Por Vitor
	// Funcaozinha besta só pra eliminar o trabalho de converter sempre as datas para o modelo DATE
	if($data && strpos($data,"/"))
	{
		$data = str_replace("/", '-', $data);
		$parts = explode("-",$data);
		$dia = $parts[0];
		$mes = $parts[1];
		$ano = $parts[2];

		$data = "$ano-$mes-$dia";
		$data = date("Y-m-d",strtotime($data)); //Porque isso? Pode ser que o $ano seja abreviado 2014  ->  "14"

	}
	return $data;
}

function to_datetime($data,$hora) //Formato YYYY-mm-dd H:i:s
{
	// Criado em 27/02/2014 Por Vitor
	// Funcaozinha besta só pra eliminar o trabalho de juntar data com hora para montar o DATETIME
	//DATA
	$data = to_date($data);

	//HORA
	$parts = explode(":",$hora);
	if(count($parts)==1)$hora = "$hora:00:00";
	if(count($parts)==2)$hora = "{$parts[0]}:{$parts[1]}:00";
	if(count($parts)==3)$hora = "{$parts[0]}:{$parts[1]}:{$parts[2]}";

	return "$data $hora";
}

function pesquisarCep($cep) //Coloca os dados na SESSION tambem
{

		$cep = only_numbers($cep);

		// OPCAO 01 - CEP FACIL  |
		#$URL = "http://www.cepfacil.com.br/service/?filiacao=DBA8E1E5-A9B9-416C-AFAE-1898ED3AD7FC&cep={$cep}&formato=json";
		// retorno= { "CEP": "70775030", "Logradouro": "SQN 316 BLOCO C", "LogradouroTipo": "QUADRA", "Cidade": "BRASÍLIA", "UF": "DF", "Bairro": "ASA NORTE", Status='valor' }

		// OPCAO 2- REPUBLICA  |
		//$URL = "http://republicavirtual.com.br/web_cep.php?cep={$cep}&formato=json";
		// retorno = {"resultado":"1","resultado_txt":"sucesso - cep completo","uf":"DF","cidade":"Bras\u00edlia","bairro":"Asa Norte","tipo_logradouro":"Quadra","logradouro":"SQN 316 Bloco C"}

		// OPCAO 2- VIACEP  |
		$URL = "http://viacep.com.br/ws/{$cep}/json/";
		/*retorno = {
				"cep": 			"71901-130",
				"logradouro": 	"Avenida Parque Águas Claras Quadra 301",
				"bairro": 		"Norte (Águas Claras)",
				"localidade": 	"Brasília",
				"uf": 			"DF",
				"ibge":			"5300108"
			}

			em caso de erro
			{
				"erro" : true
			}
		*/


		$json = file_get_contents($URL);
		$data = json_decode($json, TRUE); // transforma em ARRAY
		$data['status'] = TRUE;//Primeiro status (Deixar aqui)

		//ADAPTACAO AOS MEUS PARAMETROS "Em minusculo"
		if(isset($data['Status']))
		{
			 $data['status'] = FALSE;
			 $data['status_txt'] = $data['Status'];
		}
		if(isset($data['resultado'])) {
			$data['status']		= $data['resultado'];
			$data['status_txt'] = $data['resultado_txt'];
		}
		if(isset($data['erro'])) {
			$data['status']		= FALSE;
			$data['status_txt'] = "CEP inválido";
		}

		#TRATAMENTO DE ENDERECO (Logradouro)
		//VIACEP
		if(isset($data['localidade'])) //Somente o ViaCep informa o Localidade
		{
			$data['endereco'] 	= $data['logradouro'];
			$data['cidade'] 	= $data['localidade'];
			$data['uf']			= $data['uf'];

			# CASO BAIRRO
			# Aqui os bairros do DF são tratados de forma diferente.
			# É bem semelhante a consulta dos correios. Ex: Um cep do Gama pode sair assim: "bairro": "Setor Leste (Gama)",
			# Entao, caso tenha esses parenteses eu retiro o que está no meio deles.
			if(isset($data['bairro']))
			{
				preg_match_all("/\((.+)\)/", $data['bairro'], $matches);
				if(isset($matches[1][0]))
				{
					$data['bairro'] = $matches[1][0];
				};
			}
		}

		//CEP FACIL
		if(isset($data['Logradouro']))
		{
			$data['endereco'] = (isset($data['LogradouroTipo'])) ? $data['LogradouroTipo'] : "";
			$data['endereco'] .= (isset($data['Logradouro'])) ? " ".$data['Logradouro'] : "";

			if(isset($data['Bairro'])) 			$data['bairro']	= $data['Bairro'];
			if(isset($data['Cidade'])) 			$data['cidade']	= $data['Cidade'];
			if(isset($data['UF'])) 				$data['uf']		= $data['UF'];

			# CASO BAIRRO
			//SE FOR NO DF A [CIDADE] SEMPRE IRÁ RETORNAR  BRASILIA E O [BAIRRO] SERÁ o nome da cidade
			//EX: Bairro Guara II, Cidade Guará, UF DF
			// Será: Bairro Guará, Cidade Brasilia, UF DF
			if(strtolower($data['uf'])=="df" && strtolower(filtro_string_tira_acento($data['cidade']))!= "brasilia")
			{
				$data['bairro'] = $data['cidade'];
				$data['cidade'] = "Brasília";
			}

		}

		//REPUBLICA
		if(isset($data['tipo_logradouro']))
		{
			$data['endereco'] = (isset($data['tipo_logradouro'])) ? $data['tipo_logradouro'] : $data['endereco'];
			$data['endereco'] .= (isset($data['logradouro'])) ? " ".$data['logradouro'] : $data['endereco'];

			$data['uf']		= $data['uf'];

			# CASO BAIRRO
			//SE FOR NO DF A [CIDADE] SEMPRE IRÁ RETORNAR  BRASILIA E O [BAIRRO] SERÁ o nome da cidade
			//EX: Bairro Guara II, Cidade Guará, UF DF
			// Será: Bairro Guará, Cidade Brasilia, UF DF
			if(strtolower($data['uf'])=="df" && strtolower(filtro_string_tira_acento($data['cidade']))!= "brasilia")
			{
				$data['bairro'] = $data['cidade'];
				$data['cidade'] = "Brasília";
			}

		}

		$data['cep'] = $cep;
		if($cep==FALSE)$data['status']=FALSE;

		return $data;// Volta para json
}

function CalcCorreios($CEP_DESTINO,$PESO="0.1",$SERVICO='40010',$FORMATO=3,$COMPRIMENTO="36",$LARGURA="27",$ALTURA="18",$DIAMETRO="0",$CEP_ORIGEM="")//Webservice Correios // Valor de acordo com o CEP de origem
{
	/*
	#	DOCUMENTACAO
	#	http://www.correios.com.br/para-voce/correios-de-a-a-z/pdf/calculador-remoto-de-precos-e-prazos/manual-de-implementacao-do-calculo-remoto-de-precos-e-prazos
	#	URL exemplo pratico: http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx/CalcPreco?nCdEmpresa=&sDsSenha=&sCepOrigem=71503504&sCepDestino=70775030&nVlValorDeclarado=0&sCdAvisoRecebimento=N&nCdFormato=1&nVlComprimento=16&nVlAltura=10&sCdMaoPropria=N&nVlLargura=11&nVlDiametro=10&nCdServico=40010&nVlPeso=0.1

		# Código Serviço
		# 40010 SEDEX Varejo
		# 40045 SEDEX a Cobrar Varejo
		# 40215 SEDEX 10 Varejo
		# 40290 SEDEX Hoje Varejo
		# 41106 PAC Varejo

		# Operações
		# CalcPrecoPrazo
		# Calcpreco
		# Calcprazo

		# Formatos
		# 1 – Formato caixa/pacote
		# 2 – Formato rolo/prisma
		# 3 - Envelope
	*/
	global $INI;
	$CEP_ORIGEM = ($CEP_ORIGEM) ? $CEP_ORIGEM : $INI['cliente']['cep'];//Muito provavelmente virá VAZIO, Neste caso uso o cep original do Cliente

	$OPERACAO			= "CalcPrecoPrazo";
	$EMPRESA 			= "";
	$SENHA 				= "";
	$VALOR_DECLARADO 	= "0"; 				//Decimal ex: 10.00
	$AVISO_RECEBIMENTO	= "N";				//S,N
	$MAO_PROPRIA 		= "N"; 				//S,N
	//$SERVICO			= '40010' 			//Ver Códigos Servico / Pode ser mais de um numa consulta separados por vírgula -> "40010,41106"

	$URL = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx/{$OPERACAO}?nCdEmpresa={$EMPRESA}&sDsSenha={$SENHA}&nVlValorDeclarado={$VALOR_DECLARADO}&sCdAvisoRecebimento={$AVISO_RECEBIMENTO}&nCdFormato={$FORMATO}&nVlComprimento={$COMPRIMENTO}&nVlAltura={$ALTURA}&sCdMaoPropria={$MAO_PROPRIA}&nVlLargura={$LARGURA}&nVlDiametro={$DIAMETRO}&nCdServico={$SERVICO}&sCepOrigem={$CEP_ORIGEM}&sCepDestino={$CEP_DESTINO}&nVlPeso={$PESO}";

	$retorno = file_get_contents($URL);
	$XML = simplexml_load_string($retorno);
	#echo '<pre>' . str_replace('<', '&lt;', $retorno) . '</pre>'; //Exibe o corpo do XML
	#echo "<meta charset='utf8'>";
	#echo "<style>td,th{font-family:arial; font-size:13px;}</style>";

	$ARR_PARAMETROS = $XML->Servicos->cServico;

	//Monta uma tabela de Brinde (Não vou usar agora)
	$tabela = "<table>";
	$tabela .= "<thead>";
	$tabela .= "<tr>";
	$tabela .= "<th>Código</th>";
	$tabela .= "<th>Rota</th>";
	$tabela .= "<th>Valor</th>";
	$tabela .= "<th>ValorSemAdicionais</th>";
	$tabela .= "<th>Peso</th>";
	$tabela .= "<th>ValorMaoPropria</th>";
	$tabela .= "<th>ValorAvisoRecebimento</th>";
	$tabela .= "<th>ValorValorDeclarado</th>";
	$tabela .= "<th>PrazoEntrega</th>";
	$tabela .= "<th>EntregaDomiciliar</th>";
	$tabela .= "<th>EntregaSabado</th>";
	$tabela .= "<th>Erro</th>";
	$tabela .= "<th>MsgErro</th>";
	$tabela .= "</tr>";
	$tabela .= "</thead>";

	$tabela .= "<tbody>";

	foreach($ARR_PARAMETROS AS $cServico)
	{

		switch($FORMATO)
		{
			case "1":$FORMATO_TRADUCAO = "Caixa/pacote";break;
			case "2":$FORMATO_TRADUCAO = "Rolo/prisma";break;
			case "3":$FORMATO_TRADUCAO = "Envelope";break;
		}
		switch($cServico->Codigo)
		{
			case "40010":$CODIGO_TRADUCAO = "SEDEX";break;
			case "40045":$CODIGO_TRADUCAO = "SEDEX a Cobrar";break;
			case "40215":$CODIGO_TRADUCAO = "SEDEX 10";break;
			case "40290":$CODIGO_TRADUCAO = "SEDEX Hoje";break;
			case "41106":$CODIGO_TRADUCAO = "PAC";break;
		}


		$tabela .= "<tr>";
		$tabela .= "<td>{$cServico->Codigo} - {$FORMATO_TRADUCAO} via {$CODIGO_TRADUCAO}</td>";
		$tabela .= "<td>{$CEP_ORIGEM} >> {$CEP_DESTINO}</td>";
		$tabela .= "<td>{$cServico->Valor}</td>";
		$tabela .= "<td>{$cServico->ValorSemAdicionais}</td>";
		$tabela .= "<td>{$PESO}Kg</td>";
		$tabela .= "<td>{$cServico->ValorMaoPropria}</td>";
		$tabela .= "<td>{$cServico->ValorAvisoRecebimento}</td>";
		$tabela .= "<td>{$cServico->ValorValorDeclarado}</td>";
		$tabela .= "<td>{$cServico->PrazoEntrega}</td>";
		$tabela .= "<td>{$cServico->EntregaDomiciliar}</td>";
		$tabela .= "<td>{$cServico->EntregaSabado}</td>";
		$tabela .= "<td>{$cServico->Erro}</td>";
		$tabela .= "<td>{$cServico->MsgErro}</td>";
		$tabela .= "</tr>";
	}

	$tabela .= "</tbody>";
	$tabela .= "</table>";
	//echo $tabela;
	#$RETORNO['parametros']  = $ARR_PARAMETROS;
	#$RETORNO['tabela'] 		= $tabela;


	return (array)$ARR_PARAMETROS; //DEVOLVER SEMPRE COMO ARRAY (O SimpleXMLObject estava dando erros ao extrair/serializar)

}

function CalcDelivery($CEP) //Para valores de entrega particular
{
	$dadosEndereco 	= pesquisarCep($CEP);

	//DE ACORDO COM BAIRRO
	switch(strtolower($dadosEndereco['bairro']))
	{
		case "asa norte":
		case "asa sul":
		case "lago norte":
		case "lago sul":
		case "sudoeste":
		case "cruzeiro":
		$RETORNO['Valor'] = 30;
		$RETORNO['PrazoEntrega'] = 1;
		break;
		default:
		$RETORNO['Valor'] = 40;
		$RETORNO['PrazoEntrega'] = 2;
		break;
	}

	return $RETORNO;
}

function atualizaCarrinho()//Re-calcula os valores totais de preço e peso do carrinho, e setores de mapas
{
		global $INI;
		$VALOR 	= 0;
		$PESO 	= 0;
		$QTD 	= 0;

		@session_start();
		if(isset($_SESSION['carrinho']['produtos']))
		{

				$COPIA_CARRINHO = $_SESSION['carrinho']['produtos'];
				unset($_SESSION['carrinho']['produtos']);
				$APLICAR_TAXA = TRUE;
				$index = 0; //Vou refazer os Ids dos Carrinhos - Para evitar de ficar com chaves [0], [3], [4], [7], etc
				foreach($COPIA_CARRINHO AS $stdObjeto)
				{
					$VALOR 	+=  $stdObjeto->qtd*$stdObjeto->valor;
					$PESO 	+=  $stdObjeto->qtd*$stdObjeto->peso;
					$QTD 	+=  $stdObjeto->qtd;

					//Servicos adicionais do produto
					if(isset($stdObjeto->servicos) && is_array($stdObjeto->servicos))
					{
						foreach($stdObjeto->servicos AS $stdServico)
						{
							$VALOR 	+=  $stdServico->valor;
						}
					}

					$ARRAY_ORGANIZADO[$index] = $stdObjeto; //Aqui eu utilizo o $index para refazer as chaves. Isso ajuda a corrigir possiveis erros no carrinho
					$index++;

					if($stdObjeto->sessao->id=="3c4e8b4ea4f47f")
					{
						$APLICAR_TAXA = FALSE;
					}


				}

				$_SESSION['carrinho']['produtos'] = $ARRAY_ORGANIZADO;



				## DESCONTOS, TAXAS, FRETE, CALCULOS
				## Deve vir nessa sequencia

				//REFAZ O CALCULO DO VALOR REAL DO DESCONTO DE CUPOM - SOMENTE AQUELES DE CUPOM, CUJO A APLICACAO VAI PARA O CARRINHO COMPLETO
				if(isset($_SESSION['carrinho']['descontos']) && count($_SESSION['carrinho']['descontos'])>0)
				{
					foreach($_SESSION['carrinho']['descontos'] AS $cupom_id=>$DESCONTO)
					{
						$desconto_valor = $DESCONTO['valor'];
						$desconto_forma = $DESCONTO['forma'];

						if($desconto_forma=='porcento')$desconto_valor_real = $VALOR*$desconto_valor/100;
						else $desconto_valor_real = $desconto_valor;

						$DESCONTO['valor_real'] = $desconto_valor_real;
						$_SESSION['carrinho']['descontos'][$cupom_id] = $DESCONTO;
					}
				}




				// 1)
				// DESCONTO (DEVE VIR ANTES DO FRETE E TAXAS)
				if(isset($_SESSION['carrinho']['descontos']) && count($_SESSION['carrinho']['descontos'])>0)
				{
					foreach($_SESSION['carrinho']['descontos'] AS $cupom_id=>$DESCONTO)
					{
						$VALOR -= $DESCONTO['valor_real'];
					}
				}

				// 2)
				// SUBTOTAL (Apanhado geral de PRODUTOS E SERVICOS)
				$SUBTOTAL = $VALOR;//Preciso dessa variavel para o 'SUBTOTAL' lá embaixo


				// 3)
				// TAXA FRETE
				if(isset($_SESSION['carrinho']['frete']['valor'])) $VALOR += to_double($_SESSION['carrinho']['frete']['valor']);

				// 4)
				// TAXA CONVENIENCIA (Incrementa) - DEVE VIR DEPOS do FRETE, DESCONTOS e de todos os serviços --- O Calculo é feito em cima de tudo por causa dos descontos do PagSeguro.
				if($APLICAR_TAXA==TRUE && isset($INI['defaults']['conveniencia']) && $INI['defaults']['conveniencia']>0)
				{
					$valorConveniencia = ($VALOR * $INI['defaults']['conveniencia'])/100;
					$_SESSION['carrinho']['conveniencia_valor']	= $valorConveniencia;
					$VALOR += to_double($valorConveniencia);
				}

				//SESSIONS PARA RETORNO
				$_SESSION['carrinho']['valor']		= $VALOR;
				$_SESSION['carrinho']['peso']		= $PESO;
				$_SESSION['carrinho']['qtd']		= $QTD;
				// SUBTOTAL (LEIA-ME)
				// Valor do <tfoot> da tabela Carrinho. Entra somente (Produtos+Desconto+Servicos)

				// O 'frete' e 'Taxa de conveniencia' são contabilizados em outro lugar
				// O subtotal é uma variável sem muita relevância. Só é usada para facilitar o calculo visual do usuário na tabela
				// Mas mesmo assim achei válido utiliza-la (Valor utilizado na "removerCarrinho" da transacao.php)
				$_SESSION['carrinho']['subtotal']	= $SUBTOTAL;

		}
}

function getMapasCarrinho()//Retorna os Mapas disponiveis para escolha de lugares
{
		global $INI;
		$ARR_MAPAS = array();

		@session_start();
		if(isset($_SESSION['carrinho']['produtos']))
		{
				foreach($_SESSION['carrinho']['produtos'] AS $stdObjeto)
				{

					//DEFINE MAPA DE RESERVAS
					/*A listagem dos produtos no carrinho é linha por linha
					Já a visualizacao do mapa para reservas deve ser agrupada por setor,
					Entao vou ler os setores e agrupar para o cliente ver tudo num mapa só.
					O numero de produtos será o limite de cliques que ele terá no mapa*/
					if($stdObjeto->need_reserva)
					{
						$contagem[$stdObjeto->setor->id."_".$stdObjeto->sessao->id]++; //Genial!! Vai dizer quantas vezes essa chave se repetiu // incrementando o valor do $qtd_reservas  07/01/2015

						$arrayInfo['setor_id'] 	 = $stdObjeto->setor->id;
						$arrayInfo['nome'] 		 = $stdObjeto->setor->nome;
						$arrayInfo['mapa']		 = $stdObjeto->setor->mapa;
						$arrayInfo['produto_id'] = $stdObjeto->id;
						$arrayInfo['qtd_reservas'] = $contagem[$stdObjeto->setor->id."_".$stdObjeto->sessao->id];

						$arrayInfo['sessao_id']  = $stdObjeto->sessao->id;
						$arrayInfo['sessao_nome']  = $stdObjeto->sessao->nome;
						$arrayInfo['sessao_date']  = $stdObjeto->sessao->date;

						$ARR_MAPAS[$stdObjeto->setor->id."_".$stdObjeto->sessao->id] = $arrayInfo; //Combino  SETOR_ID+SESSAO_ID para distinguir os mapas a serem carregados
					}
				}
		}
		return $ARR_MAPAS;
}

function getFacebookData($url)//Atraves da URL, cata os parametros publicos do usuario (nome, genero, idade, cidade, etc )
{
	$data = array();

	if($url)
	{
		//Limpando 01
		$url = str_replace(array("http://","https://","www.","facebook.com","profile.php?id="),"",$url);
		$url = trim($url,"/");


		$parts = explode("/",$url); //caso sobre "vitinhoclima/about/profile" - fico só com "vitinhoclima"
		if(count($parts)>0)$url = $parts[0];

		$parts2 = explode("?",$url);//caso sobre "vitinhoclima?ref=ts" - fico só com "vitinhoclima"
		if(count($parts2)>0) $url = $parts2[0];

		$parts3 = explode("&",$url); //caso sobre "vitinhoclima&fref=pb&hc_location=friends_tab
		if(count($parts3)>0)$url = $parts3[0];

		$fields="id,name,username,first_name,last_name,link,gender,cover,picture.width(200)";
		$json = file_get_contents("https://graph.facebook.com/{$url}?fields={$fields}");
		$data = json_decode($json); // transforma em StdClass Object   --- Para converter em Array baste inserir o TRUE $data = json_decode($json,TRUE);
		$data->picture = "https://graph.facebook.com/{$url}/picture";
		$data->status = "ok";

		//Versao CURL
		/*$url = "https://graph.facebook.com/{$user}?fields={$fields}";
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  // êêêsse é o bixao. Faz com que eu possa armazenar o resultado do echo em $retorno = curl_exec($ch);
		$retorno = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($retorno,true); //O TRUE transforma em Array   $retorno['erro']['mensagem']
		*/
	}

	return $data;
}

?>
