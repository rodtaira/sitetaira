<?php
class Cliente
{

	public function __construct()
	{
		global $INI;
		$this->id 			= CLIENTE_ID;
		$this->nome 		= $INI['cliente']['nome'];
		$this->endereco 	= $INI['cliente']['endereco'];
		$this->dominio 		= $INI['cliente']['dominio'];
		$this->email 		= $INI['cliente']['email'];
		$this->telefones 	= $INI['cliente']['telefones'];
		$this->logo 		= $INI['cliente']['logo'];
		$this->historico 	= $INI['cliente']['historico'];
		$this->portfolio 	= $INI['cliente']['portfolio'];
		$this->servico 		= $INI['cliente']['servico'];
	}


	//BANNERS
	public function getBanners($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getBanners",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['banners'];
		else return FALSE;
	}
	
	//SERVICOS
	public function getServicos($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getServicos",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE)return $retorno['servicos'];
		else return FALSE;
	}
	public function getServico($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getServico",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['servico'];
		else return FALSE;
	}
	
	//COBERTURAS
	public function getCoberturas($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getCoberturas",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['coberturas'];
		else return FALSE;
	}
	public function getCobertura($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getCobertura",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['cobertura'];
		else return FALSE;
	}
	
	//DEPOIMENTOS
	public function getDepoimentos($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getDepoimentos",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['depoimentos'];
		else return FALSE;
	}
	
	//NOTICIAS
	public function getNoticias($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getNoticias",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE){
			$RETORNO['noticias'] = $retorno['noticias'];
			$RETORNO['paginas'] = $retorno['paginas'];
			return $RETORNO;
		}
		else return FALSE;
	}
	public function getNoticia($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getNoticia",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['noticia'];
		else return FALSE;
	}
	
	
	//AREA CLIENTE
	public function getPedidos($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getPedidos",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['pedidos'];
		else return FALSE;
	}
	public function getUsuario($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getUsuario",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['usuario'];
		else return FALSE;
	}
	public function getRelacionamentos($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getRelacionamentos",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE)
		{
			//$retorno['usuario']; 			// Usuario Tratado
			//$retorno['mensagem']; 		//"X relacionamentos encontrados."
			//$retorno['clientes'];			// Array de clientes - relacionamentos
			return $retorno;
		}
		else return FALSE;
	}
	public function getRetornoPagamento($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getRetornoPagamento",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['pagamento'];
		else return FALSE;
	}
	
	






	public function getVoucher($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getVoucher",$parametros);
		//$requisicao->dump = TRUE;
		return $requisicao->start();//O teste do status eu faço la na pagina, para poder recuperar a mensagem caso ocorra um erro
	}


	public function optOut($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("optOut",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		return $retorno;
	}

	public function getProdutosBeneficiario($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getProdutosBeneficiario",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['produtos'];
		else return FALSE;
	}

	
	

	public function getProximasSessoes($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getProximasSessoes",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['sessoes'];
		else return FALSE;
	}

	public function getSessao($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getSessao",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();

		if($retorno['status']==TRUE) return $retorno['sessao'];
		else return FALSE;
	}

	public function getProduto($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getProduto",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['produto'];
		else return FALSE;
	}

	public function consultaReservas($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("consultaReservas",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['reservas'];
		else return FALSE;
	}

	

	public function getEmpresaClientes($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getEmpresaClientes",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['clientes'];
		else return FALSE;
	}

	public function getEmpresaPortfolio($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getEmpresaPortfolio",$parametros);
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['portfolio'];
		else return FALSE;
	}

	public function getPedido($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getPedido",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['pedido'];
		else return FALSE;
	}

	

	//COMISSARIOS
	public function getComissario($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getComissario",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['comissario'];
		else return FALSE;
	}

	//TEXTOS (Politica Compras, Regras Gerais, Biografia, Etc)
	public function getTextos()
	{
		$requisicao = new ShowCommerceRequest("getTextos");
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno;  //Aqui vai ter $retorno['historico']; $retorno['termos_ofertas']; $retorno['politica_compras']; , etc
		else return FALSE;
	}

	//CUPOM DE DESCONTO
	public function getCupom($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getCupom",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['cupom'];
		else return FALSE;
	}

	//CARDAPIO
	public function getCardapio()
	{
		$requisicao = new ShowCommerceRequest("getCardapio");
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['cardapio'];
		else return FALSE;
	}
	public function getProdutosCardapio()
	{
		$requisicao = new ShowCommerceRequest("getProdutosCardapio");
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['produtos'];
		else return FALSE;
	}


	public function getVideos($parametros=array())
	{
		$requisicao = new ShowCommerceRequest("getVideos",$parametros);
		//$requisicao->dump = TRUE;
		$retorno = $requisicao->start();
		if($retorno['status']==TRUE) return $retorno['videos'];
		else return FALSE;
	}

	
	

}
?>
