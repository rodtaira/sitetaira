<?php
@session_start();
error_reporting(TRUE);


//HOST_EXTRA
//Já tem no URL.php mas tem que ter aqui no config tambem para quando o AJAX chamar o transacao.php
$LINHA_COMPLETA		= __FILE__;
$CONF_LOCAL_ROOT   	= $_SERVER['DOCUMENT_ROOT'];
$CONF_LOCAL_URL   	= $_SERVER['HTTP_HOST'];
$HOST_EXTRA = str_replace("\\","/",$LINHA_COMPLETA);
$HOST_EXTRA = str_replace($CONF_LOCAL_ROOT,"",$HOST_EXTRA);
$HOST_EXTRA = str_replace($CONF_LOCAL_URL,"",$HOST_EXTRA);
$HOST_EXTRA = str_replace("/SHOWCOMMERCE/php/config.php","",$HOST_EXTRA);
$HOST_EXTRA_BARRAS = str_replace("/","\\/",$HOST_EXTRA); //Acrescenta as barras invertidas

//CONSTANTE "HOST_EXTRA"
define('HOST_EXTRA'	 ,$HOST_EXTRA); //está no url.php
$URL = str_replace(HOST_EXTRA,"", $URL);
$URL = trim($URL , "/");




# 1) COOKIE
//if(isset($_SESSION['login']) && !isset($_COOKIE['USUARIO_ID']))setcookie("USUARIO_ID",$_SESSION['login']['id'],time()+3600*24*30);

# 2) CONSTANTES
define( 'CLIENTE_ID',			"5475ebb941da33"); 
define( 'URL_SHOWCOMMERCE', 	'https://showcommerce.com.br');				// nunca usar o (www), por conta do redirect no .httacess do showcommerce
define( 'VERSAO_SISTEMA',		"1.0.0");

define( 'URL_FEEDS', 			URL_SHOWCOMMERCE.'/api');
define( 'LOGIN_FACE',			URL_SHOWCOMMERCE.'/retornoLoginFacebook.php');

define('URL_SITE','http://'.$_SERVER['HTTP_HOST'].HOST_EXTRA);
define('ROOT_SITE',$_SERVER['DOCUMENT_ROOT'].HOST_EXTRA);


# 4) MAPEAMENTO DIRETORIOS
define( 'DIR_APP',				URL_SITE. 	'/SHOWCOMMERCE');
define( 'ROOT_APP',				ROOT_SITE. 	'/SHOWCOMMERCE');
define( 'DIR_PAGES',			URL_SITE.  	'');
//define( 'DIR_LIB',			URL_SITE.  	'/lib');
//define( 'ROOT_LIB',			ROOT_SITE. 	'/lib');
define( 'ROOT_PAGES',			ROOT_SITE.  '');
define( 'TRANSACAO', 			DIR_APP. 	'/php/transacao.php');


# 5) MAPEAMENTO PAGINAS //Usado pelo url.php
define( 'PAGE_LOGIN', 			URL_SITE. '/login');
define( 'PAGE_CADASTRO',		URL_SITE. '/Cadastro');
define( 'PAGE_CARRINHO',		URL_SITE. '/Carrinho');
define( 'PAGE_CHECKOUT',		URL_SHOWCOMMERCE. '/Checkout');
define( 'PAGE_CLIENTE', 		URL_SITE. '/cliente');
define( 'PAGE_EVENTOS',			URL_SITE. '/Agenda');
define( 'PAGE_FAQ',				URL_SITE. '/DuvidasFrequentes');
define( 'PAGE_RESTRITO', 		URL_SITE. '/restrito');
define( 'PAGE_LOJA', 			URL_SITE. '/Loja');
define( 'PAGE_FESTIVAL',		URL_SITE. '/Festival');
define( 'PAGE_COBERTURAS',		URL_SITE. '/Coberturas');
define( 'PAGE_VIDEOS',			URL_SITE. '/Videos');
define( 'PAGE_EMPRESA',			URL_SITE. '/Empresa');
define( 'PAGE_BLOG',			URL_SITE. '/Blog');
define( 'PAGE_SERVICOS',		URL_SITE. '/Servicos');
define( 'PAGE_SERVICO',			URL_SITE. '/Servico');
define( 'PAGE_PRODUTO', 		URL_SITE. '/Produto');
define( 'PAGE_CONTATO',			URL_SITE. '/FaleConosco');
define( 'PAGE_BUSCA',			URL_SITE. '/Busca');
define( 'PAGE_TROCAS',			URL_SITE. '/Trocas');
define( 'PAGE_PRIVACIDADE',		URL_SITE. '/Privacidade');
define( 'PAGE_ESQUECI_SENHA',	URL_SITE. '/EsqueciMinhaSenha');
define( 'PAGE_RETORNO_FACEBOOK',URL_SITE. '/retorno/facebook'); //Usado no botão de login do Face. (Gerais.func.php)


# 6) VERIFICA DISPONIBILIDADE DO SISTEMA
$cURL = curl_init(URL_SHOWCOMMERCE);
curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false); //PERMITE PUXAR USANDO LOCALHOST
curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
$resultado = curl_exec($cURL);
$info = curl_getinfo($cURL);
if($info['http_code']=="0")
{

	include(ROOT_SITE."/pages/erro/turnoff.php");
	die();
}
curl_close($cURL);


# 7) Idioma e Local
//strftime("%A, %d de %B de %Y",strtotime($Cobertura->date) );
setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
date_default_timezone_set('America/Sao_Paulo');


# 8) FUNCOES PRIMÁRIAS
include_once ROOT_APP . "/php/Gerais.func.php";


# 9) GLOBAL $INI - Requisição
global $INI;
if(!isset($_SESSION['INI']))
{
	getParams();				//PARAMETROS - Carrega os parametros e alimenta a session INI e o próprio array INI

}else{

	//echo DIR_APP . "/Gerais.func.php";
	setINI($_SESSION['INI']);	//ARRAY $INI - Atribui os valores da Session['INI'] para o array INI
}

# 10) CLIENTE (Deve vir depois do Global $INI)
$Cliente = new Cliente();

# 13) AUTOLOGIN USUARIO (Não tem SESSION mas tem COOKIE)
if(isset($_COOKIE['USUARIO_ID']) && $_COOKIE['USUARIO_ID']==true && !isset($_SESSION['login']))
{
	$parametros['usuario_id'] = $_COOKIE['USUARIO_ID'];
	$Usuario = $Cliente->getUsuario($parametros);
	loginUsuario($Usuario);
}

?>
